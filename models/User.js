const mongoose = require('../db');
const Schema = mongoose.Schema;

const trackSchema = new Schema({
    artist: String,
    title: String,
    releaseTitle: String,
    year: Number,
    releaseDate: Date,
    genre: String,
    label: String,
    playedBy: String,
    query: String,
    alias: String,
    source: String,
    cover: String,
    logo: String,
    status: Number,
    youtube: new Schema({
        id: String,
        title: String
    }),
    sample: String,
    url: String
});

const userSchema = Schema({
    _id: Schema.Types.ObjectId,
    email: String,
    password: String,
    token: String,
    playlists: [new Schema({
        _id: Schema.Types.ObjectId,
        name: [String],
        tracks: [trackSchema],
        nextScrape: Date
    })],
    news: new Schema({
        sources: [new Schema({
            name: String,
            genres: [String]
        })],
        tracks: [trackSchema],
        nextScrape: Date
    }),
    clubs: new Schema({
        queries: [String],
        tracks: [trackSchema],
        nextScrape: Date
    }),
    djs: new Schema({
        queries: [String],
        tracks: [trackSchema],
        nextScrape: Date
    }),
    producers: new Schema({
        queries: [String],
        tracks: [trackSchema],
        nextScrape: Date
    }),
    labels: new Schema({
        queries: [String],
        tracks: [trackSchema],
        nextScrape: Date
    })
});

const User = mongoose.model('User', userSchema);

module.exports = User;
