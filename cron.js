const async = require('async');
const moment = require('moment');
const User = require('./models/User');
const scrape = require('./scrape');

const userListeUpdateTracks = (force, user, action, index, arguments, cb)=>{
    if(force || user[index].nextScrape < date || !user[index].nextScrape || (user[index].nextScrape && user[index].nextScrape===null)){

        /*user[index].tracks = [];
        user.save().then(()=>{
            cb();
        });*/


        if(action === 'news'){
            scrape[action](arguments, (releases)=>{
                if(releases && releases.length > 0){
                    //on enleve les release invalides ou avec des morçeau invalide;
                    releases = releases.reduce((c, r)=>{
                        if(r && r.tracks && r.tracks.length){
                            r.tracks = r.tracks.reduce((cc, t)=>{
                                if(t){
                                    cc.push(t);
                                }
                                return cc;
                            }, []);
                            if(r.tracks.length > 0){
                                c.push(r);
                            }
                        }
                        return c;
                    }, []);
                    // ici on crée un tableau de tracks à ajouer depuis la liste des release
                    let tracksToAdd = releases.reduce((theState, release)=>{
                        if(release && release.tracks && release.tracks.length > 0){
                            release.tracks.map((t)=>{
                                t.status = 0;
                                return t;
                            });
                            theState = [...theState, ...release.tracks];
                        }
                        return theState;
                    }, []);

                    //ici on filtre si il y en qui exist deja dans cette liste
                    tracksToAdd = tracksToAdd.reduce((_state, _track)=>{
                        const exists = user[index].tracks.reduce((cc, tt)=>{
                            if(tt.alias.trim() === _track.alias.trim() || (tt.artist.trim() === _track.artist.trim() && tt.title.trim() === _track.artist.trim())){
                                cc = true;
                            }
                            return cc;
                        }, false);
                        if(exists===false){
                            _state.push(_track);
                        }
                        return _state;
                    }, []);
                    //On ajoute les nouvelles releases si il y en a
                    user[index].tracks = [...user[index].tracks, ...tracksToAdd];
                    user[index].nextScrape = (action === 'news')?moment().add(1, 'days').toDate():moment().add(5, 'days').toDate();
                    user.save().then(()=>{
                        cb();
                    }).catch((e)=>{
                        console.log(e);
                        cb();
                    });
                } else {
                    cb();
                }
            });
        } else {
            async.eachSeries(arguments, (query, cbEachArgs)=>{
                scrape[action](query, (releases)=>{
                    if(releases && releases.length > 0){
                        //on enleve les release invalides ou avec des morçeau invalide;
                        releases = releases.reduce((c, r)=>{
                            if(r && r.tracks && r.tracks.length){
                                r.tracks = r.tracks.reduce((cc, t)=>{
                                    if(t){
                                        cc.push(t);
                                    }
                                    return cc;
                                }, []);
                                if(r.tracks.length > 0){
                                    c.push(r);
                                }
                            }
                            return c;
                        }, []);
                        // ici on crée un tableau de tracks à ajouer depuis la liste des release
                        let tracksToAdd = releases.reduce((theState, release)=>{
                            if(release && release.tracks && release.tracks.length > 0){
                                release.tracks.map((t)=>{
                                    t.status = 0;
                                    return t;
                                });
                                theState = [...theState, ...release.tracks];
                            }
                            return theState;
                        }, []);

                        //ici on filtre si il y en qui exist deja dans cette liste
                        tracksToAdd = tracksToAdd.reduce((_state, _track)=>{
                            const exists = user[index].tracks.reduce((cc, tt)=>{
                                if(tt.alias.trim() === _track.alias.trim() || (tt.artist.trim() === _track.artist.trim() && tt.title.trim() === _track.artist.trim())){
                                    cc = true;
                                }
                                return cc;
                            }, false);
                            if(exists===false){
                                _state.push(_track);
                            }
                            return _state;
                        }, []);
                        //On ajoute les nouvelles releases si il y en a
                        user[index].tracks = [...user[index].tracks, ...tracksToAdd];
                        user[index].nextScrape = (action === 'news')?moment().add(1, 'days').toDate():moment().add(5, 'days').toDate();
                        user.save().then(()=>{
                            cbEachArgs();
                        }).catch((e)=>{
                            console.log(e);
                            cbEachArgs();
                        });
                    } else {
                        cbEachArgs();
                    }
                });
            }, ()=>{
                cb();
            });
        }




    } else {
        cb();
    }
};

const digswapCron = (force)=>{
    let date = Date.now();
    let query = {
        '$or': [
            {'clubs.nextScrape': {$lt: date}},
            {'clubs.nextScrape': { $exists: false, $ne: null }},
            {'clubs.nextScrape': null},
            {'artists.nextScrape': {$lt: date}},
            {'artists.nextScrape': { $exists: false, $ne: null }},
            {'artists.nextScrape': null},
            {'labels.nextScrape': {$lt: date}},
            {'labels.nextScrape': { $exists: false, $ne: null }},
            {'labels.nextScrape': null},
            {'news.nextScrape': {$lt: date}},
            {'news.nextScrape': { $exists: false, $ne: null }},
            {'news.nextScrape': null}
        ]
    };
    if(force){
        query = {}

    }
    //query = {};
    User.find(query).then((users)=>{
        async.eachSeries(users, (user, cbEachUser)=>{

            //console.log(user);
            //process.exit();


            async.waterfall([
                (newsScraperCb)=>{
                    userListeUpdateTracks(force, user, 'news', 'news', user.news.sources, newsScraperCb);
                },
                (clubsScraperCb)=>{
                    userListeUpdateTracks(force, user, 'club', 'clubs', user.clubs.queries, clubsScraperCb);
                },
                (djsScraperCb)=>{
                    userListeUpdateTracks(force, user, 'playlist', 'djs', user.djs.queries, djsScraperCb);
                },
                (producersScraperCb)=>{
                    userListeUpdateTracks(force, user, 'artist', 'producers', user.producers.queries, producersScraperCb);
                },
                (labelsScraperCb)=>{
                    userListeUpdateTracks(force, user, 'label', 'labels', user.labels.queries, labelsScraperCb);
                }
            ], ()=>{
                cbEachUser();
            });

            /*const scrapersTodo = liste.scrapers.reduce((state, scraper)=>{
                if(force || scraper.nextScrape < date || !scraper.nextScrape || (scraper.nextScrape && scraper.nextScrape===null)){
                    state.push(scraper);
                }
                return state;
            }, []);
            async.eachSeries(scrapersTodo, (scraperTodo, cbEachScrapersTodo)=>{
                if(scrape[scraperTodo.type]){
                    const argument = (scraperTodo.type === 'news')?scraperTodo.sources:scraperTodo.queries;
                    scrape[scraperTodo.type](argument, (releases)=>{
                        if(releases && releases.length > 0){

                            //on enleve les release invalides ou avec des morçeau invalide;
                            releases = releases.reduce((c, r)=>{
                                if(r && r.tracks && r.tracks.length){
                                    r.tracks = r.tracks.reduce((cc, t)=>{
                                        if(t){
                                            cc.push(t);
                                        }
                                        return cc;
                                    }, []);
                                    if(r.tracks.length > 0){
                                        c.push(r);
                                    }
                                }
                                return c;
                            }, []);

                            // ici on crée un tableau de tracks à ajouer depuis la liste des release
                            let tracksToAdd = releases.reduce((theState, release)=>{
                                if(release && release.tracks && release.tracks.length > 0){
                                    release.tracks.map((t)=>{
                                        t.status = 0;
                                        return t;
                                    });
                                    theState = [...theState, ...release.tracks];
                                }
                                return theState;
                            }, []);

                            //ici on filtre si il y en qui exist deja dans cette liste
                            tracksToAdd = tracksToAdd.reduce((_state, _track)=>{
                                const exists = liste.tracks.reduce((cc, tt)=>{
                                    if(tt.alias.trim() === _track.alias.trim() || (tt.artist.trim() === _track.artist.trim() && tt.title.trim() === _track.artist.trim())){
                                        cc = true;
                                    }
                                    return cc;
                                }, false);
                                if(exists===false){
                                    _state.push(_track);
                                }
                                return _state;
                            }, []);

                            //On ajoute les nouvelles releases si il y en a
                            liste.tracks = [...liste.tracks, ...tracksToAdd];

                            //on met à jour le nextScrape
                            liste.scrapers.map((_scraper)=>{
                                if(_scraper._id.toString() === scraperTodo._id.toString()){
                                    let nextScrape = moment().add(5, 'days').toDate();
                                    if(_scraper.type === 'news'){
                                        nextScrape = moment().add(1, 'days').toDate();
                                    }
                                    _scraper.nextScrape = nextScrape;
                                }
                                return _scraper;
                            });

                            //on save la liste
                            liste.save().then(()=>{
                                cbEachScrapersTodo();
                            }).catch(()=>{
                                cbEachScrapersTodo();
                            });

                        } else {
                            cbEachScrapersTodo();
                        }
                    });
                } else {
                    console.log('scraper callback '+scraperTodo.type+' do not exists');
                    cbEachScrapersTodo();
                }
            }, ()=>{
                cbEachListe();
            });*/
        }, ()=>{
            console.log("Cron finished");
        })
    }).catch((err)=>{
        console.log({mongooseError: err, collection: 'Liste', query: {'scrapers.nextScrape': {$lt: date}}});
        console.log("Cron finished");
    });
};

module.exports = digswapCron;
