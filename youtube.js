const md5 = require('md5');
const async = require('async');
const _ = require('underscore');
const S = require('string');
const Fuse = require('fuse.js');
const axios = require('axios');

const youtube = (q, callback)=>{
    if (q && q.length && q.length > 0) {
        q = S(q).replaceAll('(', '').s;
        q = S(q).replaceAll(')', '').s;
        q = S(q).humanize('}', '').s;
        q = q.replace(/[^\w\s]/gi, '');
        const host = 'https://www.googleapis.com';
        const path = '/youtube/v3/search?part=id%2Csnippet&key=AIzaSyDZNXZkEYA_VzTcKDk52F7WgCOcqHT1pdc&q=';
        const url = host + path + q;

        axios.get(url)
            .then(function (response) {
                let result = {html: response.data};
                if (result.html) {
                    const allResults = [];
                    const youtubeData = result.html;
                    //console.log(youtubeData);
                    if (youtubeData && youtubeData.items && youtubeData.items.length > 0 && youtubeData.items[0]) {
                        youtubeData.items.forEach((item) => {
                            if (item.id.kind === 'youtube#video') {
                                const theTitle = item.snippet.title;
                                allResults.push({ title: theTitle, id: item.id.videoId, thumbnail: item.snippet.thumbnails.default.url});
                            }
                        });
                        if (allResults.length > 0) {
                            const options = {
                                keys: ['title', 'id']
                            };
                            const fuse = new Fuse(allResults, options);
                            const fuseResult = fuse.search(q);
                            if (fuseResult.length > 0) {
                                const fuseUrlBestMatch = fuseResult[0];
                                callback({
                                    status: 'success',
                                    youtube: fuseUrlBestMatch
                                });
                            } else {
                                callback({
                                    status: 'error',
                                    message: 'no viedo found'
                                });
                            }
                        } else {
                            callback({
                                status: 'error',
                                message: 'no viedo found'
                            });
                        }
                    } else {
                        callback({
                            status: 'error',
                            message: 'no viedo found'
                        });
                    }
                } else {
                    callback({
                        status: 'error',
                        message: 'no viedo found'
                    });
                }
            }).catch(function (error) {
            callback({
                status: 'error',
                error: error
            });
        });
    } else {
        callback({
            status: 'error',
            message: 'no viedo found'
        });
    }
};

module.exports = youtube;
