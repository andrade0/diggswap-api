const md5 = require('md5');
const request = require('request');
const moment = require('moment');
const mongoose = require('./db');
const Schema = mongoose.Schema;

const cacheSchema = Schema({
    _id: Schema.Types.ObjectId,
    key: String,
    data: Schema.Types.Mixed,
    expire: Date
});

var Cache = mongoose.model('Cache', cacheSchema);

const curl = (url, callback)=>{
    console.log(url);
    Cache.findOne({key: md5(url)}).then((_cache)=>{
        let scrape = true;
        if(_cache && _cache.data && _cache.expire){
            if(moment(_cache.expire) && _cache.expire > Date.now()){
                scrape = false;
            }
        }

        if(scrape){

            Cache.deleteOne({key: md5(url)}).then(() => {
                try {
                    url = url.replace('é','e');
                    url = url.replace('è','e');
                    url = url.replace('ê','e');

                    var sleeps = [];
                    sleeps.push(0.9);
                    sleeps.push(1.8);
                    sleeps.push(1.8);
                    sleeps.push(1.2);
                    sleeps.push(1.6);
                    sleeps.push(3.6);
                    sleeps.push(2.9);
                    sleeps.push(0.95);
                    sleeps.push(1.55);
                    sleeps.push(2.1);
                    sleeps.push(1.7);
                    sleeps.push(1.5);
                    sleeps.push(1.7);

                    var user_agents = [];
                    user_agents.push("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:36.0) Gecko/20100101 Firefox/36.0");
                    user_agents.push("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0");
                    user_agents.push("Mozilla/5.0 (Windows NT 6.1; rv:15.0) Gecko/20120716 Firefox/15.0a2");
                    user_agents.push("Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; ko; rv:1.9.1b2) Gecko/20081201 Firefox/3.1b2");
                    user_agents.push("Mozilla/5.0 (Windows; U; Windows NT 5.0; it-IT; rv:1.7.12) Gecko/20050915");
                    user_agents.push("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.12 Safari/537.36 OPR/14.0.1116.4");
                    user_agents.push("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.29 Safari/537.36 OPR/15.0.1147.24 (Edition Next)");
                    user_agents.push("Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/3.0.195.27 Safari/532.0");
                    user_agents.push("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
                    user_agents.push("Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; ko; rv:1.9.1b2) Gecko/20081201 Firefox/3.1b2");
                    user_agents.push("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0");
                    user_agents.push("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
                    user_agents.push("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:36.0) Gecko/20100101 Firefox/36.0");
                    user_agents.push("Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; ko; rv:1.9.1b2) Gecko/20081201 Firefox/3.1b2");

                    var random_index = Math.floor((Math.random() * 10));
                    var user_agent = user_agents[random_index];

                    request(url, {headers: {
                            "User-Agent": user_agent,
                            "timeout": 2000
                        }}, (error, response, html) => {
                        let curl_result = {html: false, url: url};
                        if(!error)
                        {
                            curl_result = {html: html, url: url};
                        }


                        let newCache = new Cache({
                            _id: new mongoose.Types.ObjectId(),
                            key: md5(url),
                            data: curl_result,
                            expire: moment().add(5, 'days').toDate()
                        });

                        newCache.save().then((_newListe) => {
                            callback(curl_result);
                        }).catch((error)=>{
                            callback(curl_result);
                        });
                    });
                }
                catch (e) {
                    callback({html: false, url: url});
                }
            }).catch(()=>{
                callback({status: "error", message: "moogose error can't delete cache"});
            });
        } else {
            _cache.data.fromCache = true;
            callback(_cache.data);
        }
    }).catch((err)=>{
        console.log(err);
        callback({html: false, url: url});
    });
};

module.exports = curl;
