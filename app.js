const express = require('express');
const md5 = require('md5');
const async = require('async');
var fs = require('fs');
const app = express();
const randomToken = require('random-token');
const moment = require('moment');
const cron = require('node-cron');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

const bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use('/public', express.static(__dirname + '/public'));

const mongoose = require('./db');
const curl = require('./curl');
const youtube = require('./youtube');

//const Liste = require('./models/Liste');
const User = require('./models/User');
//const Playlist = require('./models/Playlist');

const digswapCron = require('./cron');



const checkToken = (req, cb)=>{
    if(req.query.token){
        User.findOne({token: req.query.token}).populate('listes').then((u)=>{
            if(u){
                cb(u);
            } else {
                cb(null);
            }
        }).catch(()=>{
            cb(null);
        });
    } else {
        /*cb(null);*/
        User.findOne({_id: "5c087bb5de7662a2e163c18d"}).populate('listes').then((u)=>{
            if(u){
                cb(u);
            } else {
                cb(null);
            }
        }).catch(()=>{
            cb(null);
        });
    }
};

app.post('/login', function (req, res) {

    if(req.body.email && req.body.password){
        User.findOne({email: req.body.email, password: md5(req.body.password)}).then((user)=>{
            if(!user){
                res.json({
                    status: 'error',
                    message: 'user not found or password is not correct'
                });
            } else {
                user.token = randomToken(16);
                user.save().then((u)=>{
                    res.json(u);
                }).catch(()=>{
                    res.json(error);
                });
            }
        }).catch((error)=>{
            res.json(error);
        });
    } else {
        res.json({
            status: 'error',
            message: 'email or password missing',
            payload: req.body
        });
    }
});

app.post('/curl', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(req.body.url){
                curl(req.body.url, (response)=>{
                    res.json(response);
                })
            } else {
                res.json({
                    status: 'error',
                    message: 'url field is missing'
                });
            }
        }
    });
});


app.get('/logout', function (req, res) {

    if(req.query.userId){
        User.findOne({_id: req.query.userId }).then((user)=>{
            if(!user){
                res.json({
                    status: 'error',
                    message: 'user not found or password is not correct'
                });
            } else {

                user.token = "";
                user.save().then((u)=>{
                    res.json(u);
                }).catch(()=>{
                    res.json(error);
                });
            }
        }).catch((error)=>{
            res.json(error);
        });
    } else {
        res.json({
            status: 'error',
            message: 'email or password missing'
        });
    }
});

app.get('/check', function (req, res) {

    res.json({
        status: 'sucess',
        message: 'API is running'
    });
});

//Check user token
app.get('/checkToken', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            res.json({
                status: 'success',
                message: 'token is ok'
            });
        }
    });
});

//Add a liste
app.post('/liste', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(req.body.title){
                let newliste = new Liste({
                    _id: new mongoose.Types.ObjectId(),
                    title: req.body.title,
                    user: user._id
                });

                if(req.body.scrapers){
                    newliste.scrapers = req.body.scrapers;
                }

                newliste.save().then((_newListe) => {
                    user.listes.push(_newListe._id);
                    user.save(_newListe._id).then(()=>{
                        res.json(_newListe);
                    }).catch(()=>{
                        res.json(error);
                    });
                }).catch((error)=>{
                    res.json(error);
                });
            } else {
                res.json({
                    status: 'error',
                    message: 'format incorrect for liste'
                });
            }
        }
    });
});

//add a playlist
app.post('/playlist', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(req.body.title){
                let newplaylist = new Playlist({
                    _id: new mongoose.Types.ObjectId(),
                    title: req.body.title,
                    user: user._id
                });
                newplaylist.save().then((_newplaylist) => {
                    user.playlists.push(_newplaylist._id)
                    user.save(_newplaylist._id).then(()=>{
                        res.json(_newplaylist);
                    }).catch(()=>{
                        res.json(error);
                    });


                }).catch((error)=>{
                    res.json(error);
                });
            } else {
                res.json({
                    status: 'error',
                    message: 'format incorrect for liste'
                });
            }
        }
    });
});

//add track to playtlist
app.post('/track', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(req.body.playlistId && req.body.trackId){
                if(error === false){
                    Playlist.findOne({_id: req.body.playlistId, user: user._id}).then((playlist)=>{
                        if(playlist){
                            if(!playlist.tracks){
                                playlist.tracks = [];
                            }
                            let alreadyExists = playlist.tracks.reduce((state, _track)=>{
                                if(_track._id.toString() === req.body.trackId.toString()){
                                    state = _track;
                                }
                                return state;
                            }, null);

                            if(alreadyExists){
                                res.json({
                                    status: 'error',
                                    message: 'track already exists in this playlist',
                                    scraper: alreadyExists
                                });
                            } else {
                                Liste.findOne({user: user._id, 'tracks._id': req.body.trackId}).then((liste)=>{

                                    if(liste){
                                        let track = liste.tracks.reduce((c, t)=>{
                                            if(t._id.toString() === req.body.trackId.toString()){
                                                c = t;
                                            }
                                            return c;
                                        }, null);

                                        if(track){
                                            playlist.tracks.push(track);
                                            playlist.save().then((_playlist) => {
                                                res.json(_playlist);
                                            }).catch((error)=>{
                                                res.json(error);
                                            });
                                        } else {
                                            res.json({
                                                status: 'error',
                                                message: 'playlist not found',
                                                payload: req.body
                                            });
                                        }
                                    } else {
                                        res.json({
                                            status: 'error',
                                            message: 'playlist not found',
                                            payload: req.body
                                        });
                                    }




                                }).catch(()=>{
                                    res.json({
                                        status: 'error',
                                        message: 'playlist not found',
                                        payload: req.body
                                    });
                                });


                            }
                        } else {
                            res.json({
                                status: 'error',
                                message: 'playlist not found',
                                payload: req.body
                            });
                        }
                    }).catch(()=>{
                        res.json(error);
                    });
                }
            } else {
                res.json({
                    status: 'error',
                    message: 'format incorrect for track or playlist',
                    payload: req.body
                });
            }
        }
    });
});


//add scraper to liste
app.post('/scraper', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {

            let errorMessages = [];

            if(!req.body.listeId){
                errorMessages.push('listeId is requiered');
            }

            if(!req.body.type){
                errorMessages.push('type is requiered');
            }

            if(['news', 'artist', 'label', 'club', 'playslist'].indexOf(req.body.type)<0){
                errorMessages.push('type has to be on of news, artist, label, club, playslist');
            }

            if(req.body.type === 'news' && !req.body.sources){
                errorMessages.push('please define sources for news');
            }

            if(req.body.type !== 'news' && !req.body.query){
                errorMessages.push('please defnie query for scraper');
            }

            if(errorMessages.length > 0){
                res.json({
                    status: 'error',
                    messages: errorMessages,
                    payload: req.body
                });
            } else {
                Liste.findOne({_id: req.body.listeId, user: user._id}).then((liste)=>{
                    if(liste){
                        if(!liste.scrapers){
                            liste.scrapers = [];
                        }
                        let alreadyExists = liste.scrapers.reduce((state, _scraper)=>{

                            if(_scraper.type === req.body.type && _scraper.query === req.body.query){
                                state = _scraper;
                            }

                            if(_scraper.type === req.body.type && _scraper.type === 'news'){
                                state = _scraper;
                            }
                            return state;
                        }, null);

                        if(alreadyExists){
                            res.json({
                                status: 'error',
                                message: 'scraper already exists',
                                scraper: alreadyExists
                            });
                        } else {
                            let nextScrape = moment().add(5, 'days').toDate();
                            if(req.body.type === 'news'){
                                nextScrape = moment().add(1, 'days').toDate();
                            }
                            const newScraper = {
                                type: req.body.type,
                                nextScrape: nextScrape
                            };
                            if(req.body.query){
                                newScraper.query = req.body.query;
                            }
                            if(req.body.sources){
                                newScraper.sources = req.body.sources;
                            }
                            liste.scrapers.push(newScraper);
                            liste.save().then((_liste) => {
                                res.json(_liste);
                            }).catch((error)=>{
                                res.json(error);
                            });
                        }
                    } else {
                        res.json({
                            status: 'error',
                            message: 'liste not found',
                            payload: req.body
                        });
                    }
                }).catch(()=>{
                    res.json(error);
                });
            }
        }
    });
});

app.patch('/liste', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(req.body.title && req.body.listeId){
                Liste.findOne({_id: req.body.listeId, user: user._id}).then((liste)=>{
                    if(liste){
                        liste.title = req.body.title;
                        liste.save().then((_liste) => {
                            res.json(_liste);
                        }).catch((error)=>{
                            res.json(error);
                        });
                    } else {
                        res.json({
                            status: 'error',
                            message: 'liste nor found',
                            payload: req.body
                        });
                    }
                }).catch((error)=>{
                    res.json(error);
                });
            } else {
                res.json({
                    status: 'error',
                    message: 'format incorrect for liste'
                });
            }
        }
    });
});



app.patch('/playlist', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(req.body.title && req.body.playlistId){
                Playlist.findOne({_id: req.body.playlistId, user: user._id}).then((playlist)=>{
                    if(playlist){
                        playlist.title = req.body.title;
                        playlist.save().then((_playlist) => {
                            res.json(_playlist);
                        }).catch((error)=>{
                            res.json(error);
                        });
                    } else {
                        res.json({
                            status: 'error',
                            message: 'liste nor found',
                            payload: req.body
                        });
                    }
                }).catch((error)=>{
                    res.json(error);
                });
            } else {
                res.json({
                    status: 'error',
                    message: 'format incorrect for liste'
                });
            }
        }
    });
});

app.patch('/scraper', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(req.body.scraperId && req.body.type && req.body.source && req.body.query !== null){

                let error = false;

                if(['news', 'artist', 'label', 'all'].indexOf(req.body.type)===-1){
                    error = true;
                    res.json({
                        status: 'error',
                        message: 'type '+req.body.type+' not allowed',
                        payload: req.body
                    });
                }

                if(['discogs', 'deejay', 'juno', 'yoyaku', 'itunes', 'beatport', 'ra', 'mixesdb'].indexOf(req.body.source)===-1){
                    error = true;
                    res.json({
                        status: 'error',
                        message: 'source '+req.body.source+' not allowed',
                        payload: req.body
                    });
                }

                if(error===false){
                    Liste.findOne({'scrapers._id': req.body.scraperId, user: user._id}).then((liste)=>{
                        if(liste){

                            liste.scrapers = liste.scrapers.map((_scraper)=>{
                                if(_scraper._id.toString() === req.body.scraperId.toString()){
                                    let nextScrape = moment().add(5, 'days').toDate();
                                    if(req.body.type === 'news'){
                                        nextScrape = moment().add(1, 'days').toDate();
                                    }
                                    _scraper.type = req.body.type;
                                    _scraper.source = req.body.source;
                                    _scraper.query = req.body.query;
                                    _scraper.nextScrape = Date.now();
                                }
                                return _scraper;
                            });

                            liste.save().then((_liste) => {
                                res.json(_liste);
                            }).catch((error)=>{
                                res.json(error);
                            });

                        } else {
                            res.json({
                                status: 'error',
                                message: 'liste not found',
                                payload: req.body
                            });
                        }
                    }).catch((error)=>{
                        console.log(error);
                        res.json(error);
                    });
                }


            } else {
                res.json({
                    status: 'error',
                    message: 'format incorrect for scraper',
                    payload: req.body
                });
            }
        }
    });
});

app.delete('/liste', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(req.body.listeId){
                Liste.findOne({_id: req.body.listeId, user: user._id}).then((liste)=>{
                    if(liste){
                        Liste.deleteOne({_id: req.body.listeId, user: user._id}).then(() => {
                            res.json({
                                status: 'success',
                                message: 'liste has been deleted',
                                payload: req.body
                            });
                        }).catch((error)=>{
                            res.json(error);
                        });
                    } else {
                        res.json({
                            status: 'error',
                            message: 'liste nor found',
                            payload: req.body
                        });
                    }
                }).catch((error)=>{
                    res.json(error);
                });
            } else {
                res.json({
                    status: 'error',
                    message: 'format incorrect for liste'
                });
            }
        }
    });
});

app.delete('/playlist', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(req.body.playlistId){
                Playlist.findOne({_id: req.body.playlistId, user: user._id}).then((liste)=>{
                    if(liste){
                        Playlist.deleteOne({_id: req.body.playlistId, user: user._id}).then(() => {
                            res.json({
                                status: 'success',
                                message: 'playlist has been deleted',
                                payload: req.body
                            });
                        }).catch((error)=>{
                            res.json(error);
                        });
                    } else {
                        res.json({
                            status: 'error',
                            message: 'playlist nor found',
                            payload: req.body
                        });
                    }
                }).catch((error)=>{
                    res.json(error);
                });
            } else {
                res.json({
                    status: 'error',
                    message: 'format incorrect for playlist'
                });
            }
        }
    });
});

app.delete('/scraper', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(req.body.scraperId){
                Liste.findOne({'scrapers._id': req.body.scraperId, user: user._id}).then((liste)=>{
                    if(liste){

                        liste.scrapers = liste.scrapers.reduce((state, _scraper)=>{
                            if(_scraper._id.toString() !== req.body.scraperId.toString()){
                                state.push(_scraper);
                            }
                            return state;
                        }, []);

                        liste.save().then((_liste) => {
                            res.json(_liste);
                        }).catch((error)=>{
                            res.json(error);
                        });

                    } else {
                        res.json({
                            status: 'error',
                            message: 'liste not found',
                            payload: req.body
                        });
                    }
                }).catch((error)=>{
                    console.log(error);
                    res.json(error);
                });
            } else {
                res.json({
                    status: 'error',
                    message: 'format incorrect for scraper',
                    payload: req.body
                });
            }
        }
    });
});

app.post('/swap', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(req.body.trackId && req.body.swapStatus)
            {
                User.findOne({_id: user._id}).then((user)=>{
                    if(user){
                        let theTrack = null;
                        let theListe = null;

                        async.waterfall([
                            (cb)=>{
                                if(user.news && user.news.tracks && user.news.tracks.length > 0){
                                    user.news.tracks = user.news.tracks.map((_track)=> {
                                        if (_track._id.toString() === req.body.trackId.toString()) {
                                            _track.status = req.body.swapStatus;
                                            theTrack = _track;
                                            theListe = 'news';
                                        }
                                        return _track;
                                    });
                                    cb();
                                } else {
                                    cb();
                                }
                            },
                            (cb)=>{
                                if(user.labels && user.labels.tracks && user.labels.tracks.length > 0){
                                    user.labels.tracks = user.labels.tracks.map((_track)=> {
                                        if (_track._id.toString() === req.body.trackId.toString()) {
                                            _track.status = req.body.swapStatus;
                                            theTrack = _track;
                                            theListe = 'labels';
                                        }
                                        return _track;
                                    });
                                    cb();
                                } else {
                                    cb();
                                }
                            },
                            (cb)=>{
                                if(user.clubs && user.clubs.tracks && user.clubs.tracks.length > 0){
                                    user.clubs.tracks = user.clubs.tracks.map((_track)=> {
                                        if (_track._id.toString() === req.body.trackId.toString()) {
                                            _track.status = req.body.swapStatus;
                                            theTrack = _track;
                                            theListe = 'clubs';
                                        }
                                        return _track;
                                    });
                                    cb();
                                } else {
                                    cb();
                                }
                            },
                            (cb)=>{
                                if(user.producers && user.producers.tracks && user.producers.tracks.length > 0){
                                    user.producers.tracks = user.producers.tracks.map((_track)=> {
                                        if (_track._id.toString() === req.body.trackId.toString()) {
                                            _track.status = req.body.swapStatus;
                                            theTrack = _track;
                                            theListe = 'producers';
                                        }
                                        return _track;
                                    });
                                    cb();
                                } else {
                                    cb();
                                }
                            },
                            (cb)=>{
                                if(user.djs && user.djs.tracks && user.djs.tracks.length > 0){
                                    user.djs.tracks = user.djs.tracks.map((_track)=> {
                                        if (_track._id.toString() === req.body.trackId.toString()) {
                                            _track.status = req.body.swapStatus;
                                            theTrack = _track;
                                            theListe = 'djs';
                                        }
                                        return _track;
                                    });
                                    cb();
                                } else {
                                    cb();
                                }
                            }
                        ], ()=>{

                            if(theTrack && theListe){
                                //on sauve la liste
                                user.save().then((newUser) => {
                                    if(user[theListe].tracks && user[theListe].tracks.length && user[theListe].tracks.length > 0){
                                        let AllUndoneTracks = user[theListe].tracks.reduce((state, track)=>{
                                            if(track.status === 0){
                                                state.push(track);
                                            }
                                            return state;
                                        }, []);
                                        let nextTrack = AllUndoneTracks[Math.floor(Math.random()*AllUndoneTracks.length)];
                                        if(nextTrack){
                                            const count = user[theListe].tracks.reduce((c, v)=>{
                                                if(v.status === 0){
                                                    c = c + 1;
                                                }
                                                return c;
                                            }, 0);
                                            const liked = user[theListe].tracks.reduce((c, v)=>{
                                                if(v.status === 1){
                                                    c = c + 1;
                                                }
                                                return c;
                                            }, 0);
                                            if(nextTrack.sample && nextTrack.sample.trim().length > 0) {
                                                res.json({count, liked, track: nextTrack});
                                            } else if(nextTrack.youtube && nextTrack.youtube.id) {
                                                res.json({count, liked, track: nextTrack});
                                            } else {
                                                youtube(nextTrack.artist+' '+nextTrack.title, (response)=>{
                                                    if(response && response.youtube){
                                                        nextTrack.youtube = response.youtube;
                                                        nextTrack.cover = response.youtube.thumbnail;
                                                        res.json({count, liked, track: nextTrack});
                                                    } else {
                                                        res.json({count, liked, track: nextTrack});
                                                    }
                                                })
                                            }
                                        } else {
                                            res.json({
                                                status: 'error',
                                                message: 'there is no next tracks',
                                                payload: req.body
                                            });
                                        }
                                    } else {
                                        res.json({
                                            status: 'error',
                                            message: 'there is no tracks in this list',
                                            payload: req.body
                                        });
                                    }
                                }).catch((error)=>{
                                    console.log(error);
                                    res.json({
                                        status: 'error',
                                        message: 'variables misssing',
                                        error: error,
                                        payload: req.body
                                    });
                                });
                            } else {
                                res.json({
                                    status: 'error',
                                    message: 'there is no tracks found in any list',
                                    payload: req.body
                                });
                            }



                        });
                    }});
            } else {
                res.json({
                    status: 'error',
                    message: 'variables misssing',
                    payload: req.body
                });
            }
        }
    });
});


app.get('/getScraperOptions', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {

            const sources = require('./digging/sources');

            let actions = ['news', 'artist', 'label', 'playedBy', 'club'];


            actions = actions.reduce((state, a)=>{

                if(!state[a]){
                    state[a] = [];
                }

                Object.keys(sources).forEach((key)=>{


                    const newsHaveGenre = sources[key].newsHaveGenre;

                    if(a==='news'){
                        if(sources[key].actions.indexOf(a)>=0){
                            state[a].push({source: key, newsHaveGenre});
                        }
                    } else {
                        if(sources[key].actions.indexOf(a)>=0){
                            state[a].push(key);
                        }
                    }
                });

                return state;
            }, {});
            res.json(actions);
        }
    });
});

app.get('/tracks', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(req.query.listeType){

                User.findOne({_id: user._id}).then((user)=>{
                    if(user){
                        if(user[req.query.listeType].tracks && user[req.query.listeType].tracks.length && user[req.query.listeType].tracks.length > 0){
                            let _tracks = user[req.query.listeType].tracks.reduce((state, track)=>{
                                if(track.status === 1){
                                    state.push(track);
                                }
                                return state;
                            }, []);
                            res.json(_tracks);
                        } else {
                            res.json({
                                status: 'error',
                                message: 'there is no tracks in this list',
                                payload: req.query
                            });
                        }
                    } else {
                        res.json({
                            status: 'error',
                            message: 'liste not found',
                            payload: req.query
                        });
                    }
                }).catch((error)=>{
                    res.json(error);
                });

            } else {
                res.json({
                    status: 'error',
                    message: 'format incorrect for swap',
                    payload: req.query
                });
            }
        }
    });
});

app.get('/track', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(req.query.trackId){

                Liste.findOne({'tracks._id': req.query.trackId, user: user._id}).then((liste)=>{
                    if(liste){
                        if(liste.tracks && liste.tracks.length && liste.tracks.length > 0){
                            let track = liste.tracks.reduce((state, track)=>{
                                if(track._id.toString() === req.query.trackId.toString()){
                                    state = track;
                                }
                                return state;
                            }, null);
                            if(track){
                                if(track.sample && track.sample.trim().length > 0) {
                                    res.json({track, lid: liste._id, title: liste.title});
                                } else if(track.youtube && track.youtube.id) {
                                    res.json({track, lid: liste._id, title: liste.title});
                                } else {
                                    youtube(track.artist+' '+track.title, (response)=>{
                                        if(response && response.youtube){
                                            track.youtube = response.youtube;
                                            track.cover = response.youtube.thumbnail;
                                            res.json({track, lid: liste._id, title: liste.title});
                                        } else {
                                            res.json({track, lid: liste._id, title: liste.title});
                                        }
                                    })
                                }
                            } else {
                                res.json({
                                    status: 'error',
                                    message: 'Track is not found',
                                    payload: req.query
                                });
                            }
                        } else {
                            res.json({
                                status: 'error',
                                message: 'there is no tracks in this list',
                                payload: req.query
                            });
                        }
                    } else {
                        res.json({
                            status: 'error',
                            message: 'liste not found',
                            payload: req.query
                        });
                    }
                }).catch((error)=>{
                    res.json(error);
                });

            } else {
                res.json({
                    status: 'error',
                    message: 'format incorrect for swap',
                    payload: req.query
                });
            }
        }
    });
});

app.post('/get', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(req.body.listeType){
                User.findOne({_id: user._id}).then((user)=>{
                    if(user[req.body.listeType]){
                        if(user[req.body.listeType].tracks && user[req.body.listeType].tracks.length && user[req.body.listeType].tracks.length > 0){
                            let AllUndoneTracks = user[req.body.listeType].tracks.reduce((state, track)=>{
                                if(track.status === 0){
                                    state.push(track);
                                }
                                return state;
                            }, []);
                            let nextTrack = AllUndoneTracks[Math.floor(Math.random()*AllUndoneTracks.length)];
                            if(nextTrack){
                                const count = user[req.body.listeType].tracks.reduce((c, v)=>{
                                    if(v.status === 0){
                                        c = c + 1;
                                    }
                                    return c;
                                }, 0);
                                const liked = user[req.body.listeType].tracks.reduce((c, v)=>{
                                    if(v.status === 1){
                                        c = c + 1;
                                    }
                                    return c;
                                }, 0);
                                if(nextTrack.sample && nextTrack.sample.trim().length > 0) {
                                    res.json({count, track: nextTrack, liked});
                                } else if(nextTrack.youtube && nextTrack.youtube.id) {
                                    res.json({count, track: nextTrack, liked});
                                } else {
                                    youtube(nextTrack.artist+' '+nextTrack.title, (response)=>{
                                        if(response && response.youtube){
                                            nextTrack.youtube = response.youtube;
                                            nextTrack.cover = response.youtube.thumbnail;
                                            res.json({count, track: nextTrack, liked});
                                        } else {
                                            res.json({count, track: nextTrack, liked});
                                        }
                                    })
                                }
                            } else {
                                res.json({
                                    status: 'error',
                                    message: 'there is no next tracks',
                                    payload: req.body
                                });
                            }
                        } else {
                            res.json({
                                status: 'error',
                                message: 'there is no tracks in this list',
                                payload: req.body
                            });
                        }
                    } else {
                        res.json({
                            status: 'error',
                            message: 'liste not found',
                            payload: req.body
                        });
                    }
                }).catch((error)=>{
                    console.log(error);
                    res.json(error);
                });

            } else {
                res.json({
                    status: 'error',
                    message: 'format incorrect for swap',
                    payload: req.body
                });
            }
        }
    });
});



app.get('/listes', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(user.listes && user.listes.length && user.listes.length > 0){
                res.json(user.listes);
            } else {
                res.json({
                    status: 'error',
                    message: 'user has no lists',
                    payload: req.body
                });
            }
        }
    });
});

app.get('/playlists', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(user.playlists && user.playlists.length && user.playlists.length > 0){
                res.json(user.playlists);
            } else {
                res.json({
                    status: 'error',
                    message: 'user has no playlists',
                    payload: req.query
                });
            }
        }
    });
});

app.get('/playlist', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(req.query.playlistId){
                Playlist.findOne({_id: req.query.playlistId}).then((playlist)=>{
                    if(playlist){
                        res.json(playlist);
                    } else {
                        res.json({
                            status: 'error',
                            message: 'playlists not found',
                            payload: req.query
                        });
                    }

                }).catch(()=>{
                    res.json({
                        status: 'error',
                        message: 'playlists not found',
                        payload: req.query
                    });
                });

            } else {
                res.json({
                    status: 'error',
                    message: 'playlists not found',
                    payload: req.query
                });
            }
        }
    });
});

app.post('/youtube', function (req, res) {
    checkToken(req, (user) => {
        if(!user){
            res.json({
                status: 'error',
                message: 'unauthorized'
            });
        } else {
            if(req.body.query) {
                youtube(req.body.query, (response)=>{
                    res.json(response);
                });
            } else {
                res.json({
                    status: 'error',
                    message: 'query is missing',
                    payload: req.body
                });
            }
        }
    });
});


app.listen(3000,  () => {
    console.log('Example app listening on port 3000!');


    /*Liste.find({'user': '5c087bb5de7662a2e163c18d'}).then((listes)=>{
        listes.forEach((liste)=>{
            if(liste.tracks && liste.tracks.length > 0){
            liste.tracks = liste.tracks.reduce((c, t)=>{
                if(t.source !== 'yoyaku'){
                    c.push(t);
                } else {
                    console.log(t);
                }
                return c;
            }, []);
            liste.save();
        }
        });
    });*/

    User.findOne({'_id': '5c087bb5de7662a2e163c18d'}).then((user)=>{
        /*console.log(user);
        user.clubs = {};
        user.clubs.nextScrape = Date.now();
        user.clubs.queries = ['concrete', 'panorama bar', 'sunwaves', 'resolute', 'dc10', 'rexclub'];
        user.artists = {};
        user.artists.queries = ['raresh', 'nicolas lutz', 'ricardo villalobos', 'seuil', 'le loup'];
        user.labels = {};
        user.labels.queries = ['perlon', 'cadenza', 'bon vivants', 'eklo'];
        user.news = {};
        user.news.sources = [{name: 'yoyaku', genres: []}, {name: 'deejay', genres: ['house']}, {name: 'juno', genres: ['house']}];
        user.save();*/
    });

});

cron.schedule('* * * * *', () => {
    //digswapCron();
});

//digswapCron(1);











