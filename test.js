const md5 = require('md5');
const async = require('async');
const moment = require('moment');
var fs = require('fs');
const curl = require('./curl');
const youtube = require('./youtube');

const scraper = require('./scrape');

/*sources.mixesdb.all("concrete", (releases)=>{
    console.log(releases);
});*/


/*scraper.news([{name: 'juno', genres: []}], (releases)=>{
   releases.forEach((r)=>{
       console.log(r.cover);
   });
});*/

const Liste = require('./models/Liste');

Liste.find({}).then((listes)=>{
    const validTracks = [];
    async.eachSeries(listes, (liste, cb)=>{
        liste.tracks.forEach((t)=>{
            if(t.status === 1){
                validTracks.push(t);
            }
        });
        cb();
    }, ()=>{
        fs.writeFile('goodTracks.json', JSON.stringify(validTracks), 'utf8', ()=>{
            console.log('ok');
        });
    });

});
