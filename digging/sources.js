const Juno = require('./sources/juno');
const Ra = require('./sources/ra');
const Deejay = require('./sources/deejay');
const Itunes = require('./sources/itunes');
const Beatport = require('./sources/beatport');
const Discogs = require('./sources/discogs');
const Mixesdb = require('./sources/mixesdb');
const Youtube = require('./sources/youtube');
const Yoyaku = require('./sources/yoyaku');

const sourcesObjects = {};
sourcesObjects.juno = new Juno();
sourcesObjects.ra = new Ra();
sourcesObjects.deejay = new Deejay();
//sourcesObjects.itunes = new Itunes();
sourcesObjects.beatport = new Beatport();
//sourcesObjects.discogs = new Discogs();
sourcesObjects.mixesdb = new Mixesdb();
//sourcesObjects.youtube = new Youtube();
sourcesObjects.yoyaku = new Yoyaku();

module.exports = sourcesObjects;
