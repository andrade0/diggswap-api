const  curl = require('../../curl');

module.exports = class discogs {
  constructor() {
    this.name = 'Discogs';
    this.value = 'discogs';
    this.logo = 'public/img/sources/discogs.png';
    this.color = '#333333';
    this.actions = ['artist', 'label'];
  }
  artist(query, searchArtistCallback) {
    curl("https://us-central1-digging-903.cloudfunctions.net/searchDiscogs?artist="+query, function(result){
      if(result && result.html){
        let releases = result.html.reduce((state, r)=>{
          const release = diggingHelpers.prepareRelease(this, r);
          if(release){
            state.push(release);
          }
          return state;
        }, []);
        searchArtistCallback(JSON.parse(result.html));
      } else {
        searchArtistCallback([]);
      }
    });
  }
  label(query, searchArtistCallback) {
    curl("https://us-central1-digging-903.cloudfunctions.net/searchDiscogs?label="+query, function(result){
      if(result && result.html){
        searchArtistCallback(JSON.parse(result.html));
      } else {
        searchArtistCallback([]);
      }
    });
  }
  all(query, searchArtistCallback) {
    curl("https://us-central1-digging-903.cloudfunctions.net/searchDiscogs?query="+query, function(result){
      if(result && result.html){
        searchArtistCallback(JSON.parse(result.html));
      } else {
        searchArtistCallback([]);
      }
    });
  }
  artist1(query, page, searchArtist1Callback) {
    curl("https://us-central1-digging-903.cloudfunctions.net/discogsGetReleaseDetails?rid="+page.id, function(result){
      if(result && result.html && JSON.parse(result.html).id){
        searchArtist1Callback(JSON.parse(result.html));
      } else {
        searchArtist1Callback(null);
      }
    });
  }
  label1(query, page, searchArtist1Callback) {
    curl("https://us-central1-digging-903.cloudfunctions.net/discogsGetReleaseDetails?rid="+page.id, function(result){
      if(result && result.html && JSON.parse(result.html).id){
        searchArtist1Callback(JSON.parse(result.html));
      } else {
        searchArtist1Callback(null);
      }
    });
  }
  all1(query, page, searchArtist1Callback) {
    curl("https://us-central1-digging-903.cloudfunctions.net/discogsGetReleaseDetails?rid="+page.id, function(result){
      if(result && result.html && JSON.parse(result.html).id){
        searchArtist1Callback(JSON.parse(result.html));
      } else {
        searchArtist1Callback(null);
      }
    });
  }
  /*parseDetail(html, page_url) {
    const $ = cheerio.load(html);
    let release = null;
    const artists_list = [];
    $('#profile_title > span:nth-child(1) > span').each(function(){
      artists_list.push($(this).find('a').text());
    });
    const genres_list = [];
    $('#page_content > div.body > div.profile a').each(function(){
      if($(this).attr('href').match(new RegExp('style', 'i'))){
        genres_list.push($(this).find('a').text());
      }
    });
    const releaseArtist = artists_list.join(', ');
    const releaseCover = $('#page_content > div.body > div.image_gallery.image_gallery_large > a > span.thumbnail_center > img').attr('src');
    const releaseTitle = $('#profile_title > span:nth-child(2)').text();
    const releaseLabel = $('#page_content > div.body > div.profile > div:nth-child(3) > a').text();
    const releaseGenre = genres_list.join(', ');
    const dateField = $('#page_content > div.body > div.profile > div:nth-child(9) > a').text().trim();
    const dateFieldValues = dateField.split(' ');
    let releaseYear = null;
    let releaseDate = null;
    const months = [];
    months.push('');
    months.push('Jan');
    months.push('Feb');
    months.push('Mar');
    months.push('Apr');
    months.push('May');
    months.push('Jun');
    months.push('Jul');
    months.push('Aug');
    months.push('Sep');
    months.push('Oct');
    months.push('Nov');
    months.push('Dec');
    let monthIndex = null;
    let month = null;
    switch(dateFieldValues.length) {
      case 1:
        if(Number(dateFieldValues[0]) > 1000) {
          releaseYear = dateFieldValues[0];
        }
        break;
      case 2:
        if(Number(dateFieldValues[1]) > 1000) {
          releaseYear = dateFieldValues[1];
        }
        if(months.indexOf(dateFieldValues[0])>=0)
        {
          monthIndex = months.indexOf(dateFieldValues[0]);
          if(monthIndex.toString().length === 1) {
            month = '0'+monthIndex;
          } else {
            month = monthIndex.toString();
          }
          if(releaseYear && month) {
            releaseDate = releaseYear+'-'+month+'-01';
          }
        }
        break;
      case 3:
        if(Number(dateFieldValues[2]) > 1000) {
          releaseYear = dateFieldValues[2];
        }
        let day = null;
        if(Number(dateFieldValues[0]) > 0) {
          day = dateFieldValues[0];
        }
        monthIndex = months.indexOf(dateFieldValues[1]);
        if(monthIndex.toString().length === 1) {
          month = '0'+monthIndex;
        } else {
          month = monthIndex.toString();
        }
        if(releaseYear && month && day) {
          releaseDate = releaseYear+'-'+month+'-'+day;
        }
        break;
      default:
        break;
    }

    const tracks = [];
    $('#tracklist').find('table.playlist').find('tr.track').each(function(){
      let trackArtist = $(this).find('td.tracklist_track_artists a').text().trim();
      const tracktitle = $(this).find('td.tracklist_track_title span.tracklist_track_title').text().trim();
      if(trackArtist.length === 0) {
        trackArtist = releaseArtist;
      }
      tracks.push({
        artist: trackArtist,
        title: tracktitle,
        sample: null
      });
    });

    release = {
      kind: 'release',
      artist: releaseArtist,
      tracks,
      label: releaseLabel,
      cover: releaseCover,
      source: 'discogs',
      url: page_url,
      title: releaseTitle,
      playedBy: releaseArtist,
      year: releaseYear,
      genre: releaseGenre,
      releaseDate
    };

    return release;
  }
  label(query, href, searchArtistCallback) {

    curl("https://us-central1-digging-903.cloudfunctions.net/searchDiscogs?label="+query, function(error, result){
      if(result && result.html){
        searchArtistCallback(result.html);
      } else {
        searchArtistCallback([]);
      }
    });
  }
  label1(query, page, searchArtist1Callback) {
    curl("https://us-central1-digging-903.cloudfunctions.net/discogsGetReleaseDetails?rid="+page.id, function(error, result){
      if(result && result.html && result.html.id){
        searchArtist1Callback(result.html);
      } else {
        searchArtist1Callback(null);
      }
    });
  }
  release(url, callback) {
    const $this = this;
    curl(url, (error, result) => {
      if (result.html) {
        const r = $this.parseDetail(result.html, url);
        if (r) {
          const release = diggingHelpers.prepareRelease($this, r);
          callback(release);
        } else {
          callback(null);
        }
      } else {
        callback(null);
      }
    });
  }
  suggestion(track, callback) {
    const $this = this;
    curl(`https://www.discogs.com/fr/search/?q=${encodeURI(`${track.artist} ${track.title}`)}&type=release`, (error, result) => {
      if (result.html) {
        const $ = cheerio.load(result.html);

        const href = $('a.search_result_title').first().attr('href');

        if (href) {
          const expl = $('a.search_result_title').first().attr('href').split('/');

          const id = expl[expl.length - 1];

          curl(`https://www.discogs.com/release/recs/${id}?type=release&page=1`, (error, result) => {
            const r = $this.parse_suggestions(result.html, (releases) => {
              if (releases) {
                callback(releases);
              } else {
                callback([]);
              }
            });
          });
        } else {
          callback([]);
        }
      } else {
        callback([]);
      }
    });
  }
  parse_suggestions(html, callback) {
    const $this = this;
    const $ = cheerio.load(html);
    const releaseUrl = $('div.card').first().find('a.thumbnail_link').first().attr('href');

    const releases = [];
    async.each($('div.card'), (element, cb) => {
      const releaseUrl = $(element).find('a.thumbnail_link').first().attr('href');
      const suggestion_url = `https://www.discogs.com${releaseUrl}`;
      curl(suggestion_url, (error, result) => {
        if (result.release) {
          releases.push(release);
          cb(null);
        } else if (result.html) {
          const r = $this.parseDetail(result.html, suggestion_url);
          if (r) {
            const release = diggingHelpers.prepareRelease($this, r);
            releases.push(release);
            cb(null);
          } else {
            cb(null);
          }
        } else {
          cb(null);
        }
      });
    }, (e, r) => {
      callback(releases);
    });
  } /* ,
   getNews: function(query, getNewsCallback){
   const today = moment().format("MMM")+moment().format("Do")+moment().format("YY");
   var $this = this;
   var pages = [];
   async.waterfall([
   function(waterfallCallback)
   {
   var url = "https://www.discogs.com/?limit=50&page=1&ddk="+today;
   curl(url, waterfallCallback);
   },
   function(result ,waterfallCallback)
   {
   if(result.html)
   {
   var $ = cheerio.load(result.html);

   $('#search_results div.card > a').each(function(k, v){
   var url = 'https://www.discogs.com'+$(v).attr('href');
   pages.push({discogs_id: null, url: url});
   });

   waterfallCallback(null);
   }
   else
   {
   waterfallCallback(true);
   }
   },
   function(waterfallCallback)
   {
   curl(page.url,  waterfallCallback);
   },
   function(result, waterfallCallback)
   {
   if(result.html)
   {
   var r = $this.parseDetail(result.html, page.url);
   if(r)
   {
   var release = diggingHelpers.prepareRelease($this, r);

   if(release)
   {
   waterfallCallback(release)
   }
   else
   {
   waterfallCallback(null);
   }
   }
   else
   {
   waterfallCallback(null);
   }
   }
   else
   {
   waterfallCallback(null);
   }
   }
   ], function(err){
   console.log(pages);
   //getNewsCallback(pages);
   });
   }*/
};
