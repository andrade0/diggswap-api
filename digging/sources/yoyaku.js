const  Fuse = require('fuse.js');
const  _ = require('underscore');
const  async = require('async');
const  S = require('string');
const  cheerio = require('cheerio');
const  moment = require('moment');
const  curl = require('../../curl');
const  diggingHelpers = require('../diggingHelpers');

module.exports = class yoyaku {
    constructor() {
        this.name = 'Yoyaku';
        this.newsHaveGenre = false;
        this.value = 'yoyaku';
        this.logo = 'public/img/sources/yoyaku.png';
        this.color = '#271F22';
        this.actions = ['artist', 'label', 'news'];
    }
    artist(query, searchArtistCallback) {
        const $this = this;

        curl(`https://www.yoyaku.io/?s=${encodeURI(query)}&post_type=product&tags=1&ixwps=1`, (result) => {
            if (result.html) {
                const $ = cheerio.load(result.html);

                const releases = [];

                $('ul.products li.product').each(function () {
                    const r = {
                        kind: 'release',
                        item_type: 'release',
                        playedBy: null,
                        year: null,
                        releaseDate: null,
                        genre: 'house'
                    };
                    r.url = $(this).find('a.woocommerce-LoopProduct-link').attr('href');
                    r.cover = `http:${$(this).find('.image-and-action-button img.wp-post-image').attr('src')}`;
                    r.title = $(this).find('div.yoyaku-desc a.woocommerce-LoopProduct-link h2').text();
                    r.artist = $(this).find('div.yoyaku-desc a.woocommerce-LoopProduct-link div.artist span').text();
                    r.label = $(this).find('div.yoyaku-desc a.woocommerce-LoopProduct-link div.label span').text();
                    r.tracks = [];

                    if (r.artist.trim().length <= 0) {
                        r.artist = 'unknown';
                    }

                    $(this).find('div.yoyaku-desc div.action_button div.player div.fap-in-page-playlist-hidden span.fap-single-track').each(function () {
                        const title_str = $(this).attr('data-title');
                        const expl = title_str.split('&ndash;');
                        if (expl.length > 1) {
                            r.tracks.push({
                                artist: expl[0].trim().substr(4),
                                title: expl[1].trim(),
                                sample: $(this).attr('data-href')
                            });
                        } else {
                            r.tracks.push({
                                artist: r.artist,
                                title: title_str.trim().substr(4),
                                sample: $(this).attr('data-href')
                            });
                        }
                    });

                    if (r) {
                        const release = diggingHelpers.prepareRelease($this, r);

                        if (release) {
                            releases.push(release);
                        }
                    }
                });


                searchArtistCallback(releases);
            } else {
                searchArtistCallback([]);
            }
        });
    }
    parseDetail(html, page_url) {
        const $ = cheerio.load(html);
        let release = null;
        const artists_list = [];
        const releaseCover = $('.attachment-shop_single.size-shop_single.wp-post-image').attr('src');
        const releaseTitle = $('.summary.entry-summary .title.table span.results').text();
        const releaseLabel = $('.summary.entry-summary .label.table span.results a').text();
        const releaseArtist = [];
        $('.summary.entry-summary .artist.table span.results a').each(function () {
            releaseArtist.push($(this).text());
        });
        const releaseYear = null;
        const releaseDate = null;
        const releaseGenre = [];
        $('.summary.entry-summary .style.table span.results a').each(function () {
            releaseGenre.push($(this).text());
        });

        const tracks = [];
        $('.yoyaku-single-product-player .yoyaku-player-single-product span.fap-single-track').each(function () {
            const title_str = $(this).attr('data-title');//

            const expl = title_str.split('&ndash;');

            if (expl.length > 1) {
                tracks.push({
                    artist: expl[0].trim().substr(4),
                    title: expl[1].trim(),
                    sample: $(this).attr('data-href')
                });
            } else {
                tracks.push({
                    artist: releaseArtist.join(''),
                    title: title_str.trim().substr(4),
                    sample: $(this).attr('data-href')
                });
            }
        });

        release = {
            artist: releaseArtist.join(', '),
            title: releaseTitle,
            label: releaseLabel,
            genre: releaseGenre.join(', '),
            tracks,
            url: page_url,
            kind: 'release',
            source: 'yoyaku',
            cover: releaseCover,
            playedBy: null,
            releaseDate: null,
            year: null
        };

        return release;
    }
    label(query, searchArtistCallback) {
        this.artist(query, searchArtistCallback);
    }
    all(query, searchArtistCallback) {
        this.artist(query, searchArtistCallback);
    }
    release(url, callback) {
        const $this = this;
        curl(url, (result) => {
            if (result.html) {
                const r = $this.parseDetail(result.html, url);
                if (r) {
                    const release = diggingHelpers.prepareRelease($this, r);
                    callback(release);
                } else {
                    callback(null);
                }
            } else {
                callback(null);
            }
        });
    }
    news(getNewsCallback) {
        const $this = this;
        const today = moment().format("MMM")+moment().format("Do")+moment().format("YY");
        /*const feeds = [
            `https://www.yoyaku.io/all-stock/?ddk=${today}`,
            `https://www.yoyaku.io/style/techno/?ddk=${today}`,
            `https://www.yoyaku.io/style/minimal-techno/?ddk=${today}`,
            `https://www.yoyaku.io/style/techno/?ddk=${today}`
        ];*/

        const feeds = [
            `https://www.yoyaku.io/style/minimal-techno/?ddk=${today}`,
            `https://www.yoyaku.io/forthcoming/?ddk=${today}`,
        ];

        async.reduce(feeds, [], function(releases, url, callback) {
            curl(url, (result) => {
            if (result && result.html) {
                const $ = cheerio.load(result.html);
                $('ul.products li.product').each(function () {
                    const r = {
                        kind: 'release',
                        item_type: 'release',
                        playedBy: null,
                        year: null,
                        releaseDate: null,
                        genre: null
                    };
                    r.url = $(this).find('a.woocommerce-LoopProduct-link').attr('href');
                    r.cover = $(this).find('.size-woocommerce_thumbnail').attr('src');
                    r.title = $(this).find('div.yoyaku-desc a.woocommerce-LoopProduct-link h2').text();
                    r.artist = $(this).find('div.yoyaku-desc a.woocommerce-LoopProduct-link div.artist span').text();
                    r.label = $(this).find('div.yoyaku-desc a.woocommerce-LoopProduct-link div.label span').text();
                    r.tracks = [];
                    $(this).find('div.yoyaku-desc div.action_button div.player div.fap-in-page-playlist-hidden span.fap-single-track').each(function () {
                        const title_str = $(this).attr('data-title');
                        const expl = title_str.split('&ndash;');

                        let _artist = null;
                        let _title = null;
                        let trackIndexInEp = null;
                        let sample = null;

                        if (expl.length > 1) {
                            _artist = expl[0].trim().substr(4);
                            _title = expl[1].trim();
                            trackIndexInEp = expl[0].split(' ')[0].trim().replace('.', '');
                        } else {
                            _artist = r.artist;
                            _title = title_str.trim().substr(3);
                            trackIndexInEp = title_str.trim().split(' ')[0].trim().replace('.', '');
                        }
                        let splitUrl = r.url.trim().split('/');
                        sample = 'https://player.yoyaku.io/mp3/'+splitUrl[splitUrl.length-2].toUpperCase()+'_'+trackIndexInEp+'.mp3';
                        r.tracks.push({
                                artist: _artist,
                                title: _title,
                                sample
                            });
                    });
                    if (r) {
                        const release = diggingHelpers.prepareRelease($this, r);
                        if (release) {
                            releases.push(release);
                        }
                    }
                });

                callback(null, releases);
            } else {
                callback(null, releases);
            }
        });
        }, function(err, releases) {
            getNewsCallback(releases);
        });
    }
};
