const  async = require('async');
const  S = require('string');
const  cheerio = require('cheerio');
const  moment = require('moment');
const  curl = require('../../curl');
const  diggingHelpers = require('../diggingHelpers');

module.exports = class beatport {

  constructor() {
    this.name = 'Beatport';
    this.newsHaveGenre = true;
    this.value = 'beatport';
    this.logo = 'public/img/sources/beatport.png';
    this.color = '#B8E000';
    this.actions = ['artist', 'label'];
  }
  artist(query, searchArtistCallback) {
    const releases = [];
    async.waterfall([
      (waterfallCallback) => {
        const url = `https://www.beatport.com/search?q=${encodeURI(query)}`;
        curl(url, (result)=>{
          waterfallCallback(null, result);
        });
      },
      (result, waterfallCallback) => {
        let url = null;
        if (result.html) {
          const beatportResults = [];
          const $ = cheerio.load(result.html);
          $('main.all-search-results div.artists ul.bucket-items li.bucket-item > a').each((k, v) => {
            const title = $(v).find('p').text();
            const href = `https://www.beatport.com${$(v).attr('href')}/tracks?per-page=150&sort=release-desc`;
            beatportResults.push({title: title.toLowerCase().trim(), href});
          });
          const options = {
            keys: ['title'],
            id: 'href'
          };
          const fuse = new Fuse(beatportResults, options);
          const fuseResult = fuse.search(query);

          if (fuseResult.length > 0 && fuseResult[0]) {
            url = fuseResult[0];
          }

          waterfallCallback(null, url);
        } else {
          waterfallCallback(true);
        }
      },
      (url, waterfallCallback) => {
        if (url) {
          curl(url, (result)=>{
          waterfallCallback(null, result);
          });
        } else {
          waterfallCallback(true);
        }
      },
      (result, waterfallCallback) => {
        let releasesFromSource = [];
        if (result.html) {
          releasesFromSource = this.parseList(result.html);
        }
        if (releasesFromSource.length > 0) {
          async.each(releasesFromSource, (r, eachCallback) => {
            const release = diggingHelpers.prepareRelease(this, r);

            if (release) {
              releases.push(release);
            }
            eachCallback(null);
          }, () => {
            waterfallCallback(null);
          });
        } else {
          waterfallCallback(null);
        }
      }], () => {
      searchArtistCallback(releases);
    });
  }
  label(query, searchArtistCallback) {
    const releases = [];
    async.waterfall([
      (waterfallCallback) => {
        const url = `https://www.beatport.com/search?q=${encodeURI(query)}`;
        curl(url, (result)=>{
          waterfallCallback(null, result);
        });
      },
      (result, waterfallCallback) => {
        let url = null;
        if (result.html) {
          const beatportResults = [];
          const $ = cheerio.load(result.html);
          $('main.all-search-results div.labels ul.bucket-items li.bucket-item > a').each((k, v) => {
            const title = $(v).find('p').text();
            const href = `https://www.beatport.com${$(v).attr('href')}/tracks?per-page=150&sort=release-desc`;
            beatportResults.push({title: title.toLowerCase().trim(), href});
          });
          const options = {
            keys: ['title'],
            id: 'href'
          };

          const fuse = new Fuse(beatportResults, options);
          const fuseResult = fuse.search(query);

          if (fuseResult.length > 0 && fuseResult[0]) {
            url = fuseResult[0];
          }

          if (url) {
            waterfallCallback(null, url);
          } else {
            waterfallCallback(true);
          }
        } else {
          waterfallCallback(true);
        }
      },
      (url, waterfallCallback) => {
        curl(url, (result)=>{
          waterfallCallback(null, result);
        });
      },
      (result, waterfallCallback) => {
        let releasesFromSource = [];
        if (result.html) {
          releasesFromSource = this.parseList(result.html);
        }
        if (releasesFromSource.length > 0) {
          async.each(releasesFromSource, (r, eachCallback) => {
            const release = diggingHelpers.prepareRelease(this, r);

            if (release) {
              releases.push(release);
            }
            eachCallback(null);
          }, () => {
            waterfallCallback(null);
          });
        } else {
          waterfallCallback(null);
        }
      }], () => {
      searchArtistCallback(releases);
    });
  }
  all(query, searchArtistCallback) {
    const releases = [];
    async.waterfall([
      (waterfallCallback) => {
        const url = `https://www.beatport.com/search/tracks?q=${encodeURI(query)}&per-page=150`;
        curl(url, (result)=>{
          waterfallCallback(null, result);
        });
      },
      (result, waterfallCallback) => {
        let releasesFromSource = [];
        if (result.html) {
          releasesFromSource = this.parseList(result.html);
        }
        if (releasesFromSource.length > 0) {
          async.each(releasesFromSource, (r, eachCallback) => {
            const release = diggingHelpers.prepareRelease(this, r);

            if (release) {
              releases.push(release);
            }
            eachCallback(null);
          }, () => {
            waterfallCallback(null);
          });
        } else {
          waterfallCallback(null);
        }
      }], () => {
      searchArtistCallback(releases);
    });
  }
  parseList(html) {
    const $ = cheerio.load(html);
    const str = $('div#wrapper div#wrapcontent.column div#content h1.inline').first().text().trim();
    if (str) {
      const split = str.split(' : ');
      if (split && split.length && split.length === 2) {
        const query = split[1];
      }
    }
    const releases = [];
    const releasesIndex = {};
    const allTracks = [];

    if ($('section.page-content-container div.tracks ul.bucket-items li.bucket-item').length > 0) {
      $('section.page-content-container div.tracks ul.bucket-items li.bucket-item').each(function () {
        let releaseTitle = null;
        let trackTitle = null;
        let releaseYear = null;
        let releaseDate = null;
        let releaseCover = null;
        let releaseLabel = null;
        let releaseUrl = null;
        let releaseGenre = null;
        let trackArtist = null;
        let releaseSourceId = null;
        const trackSourceId = $(this).attr('data-ec-id');
        const trackSample = `https://geo-samples.beatport.com/lofi/${trackSourceId}.LOFI.mp3`;

        const artistList = [];

        $(this).find('p.buk-track-artists a').each(function () {
          artistList.push($(this).text().trim());
        });


        const l = $(this).find('p.buk-track-labels a').text().trim();
        if (l) {
          releaseLabel = l;
        }

        const t = $(this).find('div.buk-track-artwork-parent a').first();
        if (t) {
          releaseUrl = `https://www.beatport.com${t.attr('href')}`;
          const tab = t.attr('href').split('/');
          let title = tab[tab.length - 2];
          title = S(title).replaceAll('-', ' ').s;
          releaseTitle = S(title).capitalize().s.trim();
          releaseCover = t.find('img').attr('data-src');
          releaseSourceId = tab[tab.length - 1];
        }

        const gs = [];
        $(this).find('p.buk-track-genre a').each((k, v) => {
          const a = $(v);
          gs.push(a.text());
        });

        if (gs.length > 0) {
          releaseGenre = gs.join(', ');
        }

        const rd = $(this).find('p.buk-track-released').text().trim().split('-');

        if (rd && rd.length && rd.length === 3) {
          releaseYear = rd[0];
          const month = rd[1];
          const day = rd[2];
          releaseDate = `${releaseYear}-${month}-${day}`;
        }

        trackArtist = artistList;


        if ($(this).find('p.buk-track-title a *')) {
          const str = [];
          $(this).find('p.buk-track-title a *').each(function() {
            str.push($(this).text().trim());
          });
          trackTitle = str.join(' ');
        }

        if (trackSourceId && releaseSourceId && trackTitle) {
          allTracks.push({
            releaseTitle,
            trackTitle,
            releaseYear,
            releaseDate,
            releaseCover,
            releaseLabel,
            releaseUrl,
            releaseGenre,
            trackArtist,
            releaseSourceId,
            trackSourceId,
            trackSample
          });
        }
      });
    }

    allTracks.forEach((t) => {
      const rid = t.releaseSourceId;

      if (!releasesIndex[rid]) {
        releasesIndex[rid] = {
          releaseTitle: t.releaseTitle,
          releaseYear: t.releaseYear,
          releaseDate: t.releaseDate,
          releaseCover: t.releaseCover,
          releaseLabel: t.releaseLabel,
          releaseUrl: t.releaseUrl,
          releaseGenre: t.releaseGenre,
          releaseSourceId: t.releaseSourceId
        };
        releasesIndex[rid].tracks = [];
      }
      releasesIndex[rid].tracks.push({
        artist: t.trackArtist,
        sample: t.trackSample,
        title: t.trackTitle
      });
    });

    for (const rid in releasesIndex) {
      const r = releasesIndex[rid];
      if (r.tracks) {
        const releaseArtistList = [];

        r.tracks.forEach((t, kk) => {
          const artist = t.artist;

          releasesIndex[rid].tracks[kk].artist = artist.join(', ');

          artist.forEach((theArtist, k) => {
            if (!releaseArtistList.indexOf(theArtist)) {
              releaseArtistList.push(theArtist);
            }
          });
        });

        if (releaseArtistList.length > 1) {
          releasesIndex[rid].releaseArtist = 'Various Artists';
        } else {
          releasesIndex[rid].releaseArtist = releaseArtistList[0];
        }
      }
    }

    for (const rid in releasesIndex) {
      const rr = releasesIndex[rid];
      releases.push({
        kind: 'release',
        artist: rr.releaseArtist,
        tracks: rr.tracks,
        label: rr.releaseLabel,
        cover: rr.releaseCover,
        source: this,
        url: rr.releaseUrl,
        title: rr.releaseTitle,
        playedBy: null,
        year: rr.releaseYear,
        releaseDate: rr.releaseDate,
        genre: rr.releaseGenre
      });
    }

    return releases;
  }
  news(getNewsCallback) {
    const today = moment().format("MMM")+moment().format("Do")+moment().format("YY");
    const feeds = [
      `https://www.beatport.com/tracks/all?per-page=150&ddk=${today}`,
      `https://www.beatport.com/tracks/all?per-page=150&genres=38&ddk=${today}`,
      `https://www.beatport.com/tracks/all?per-page=150&genres=5&ddk=${today}`
    ];

    const allBeatport = [];
    async.each(feeds, (url, cb) => {
      curl(url, (result) => {
        if (result && result.html) {
          const releases = this.parseList(result.html);

          if (releases && releases.length && releases.length > 0) {
            releases.forEach((r) => {
              const release = diggingHelpers.prepareRelease(this, r);
              if (release) {
                allBeatport.push(release);
              }
            });
            cb(null);
          } else {
            cb(null);
          }
        } else {
          cb(null);
        }
      });
    }, (e, r) => {
      getNewsCallback(allBeatport);
    });
  }
};
