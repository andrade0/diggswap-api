const  Fuse = require('fuse.js');
const  _ = require('underscore');
const  async = require('async');
const  S = require('string');
const  cheerio = require('cheerio');
const  moment = require('moment');
const  curl = require('../../curl');
const  diggingHelpers = require('../diggingHelpers');

module.exports = class itunes {
  constructor() {
    this.name = 'Itunes';
    this.value = 'itunes';
    this.logo = 'public/img/sources/itunes.png';
    this.color = '#F3F1F2';
    this.actions = ['artist'];
  }
  artist(query, searchArtistCallback) {
    const $this = this;
    const releases = [];
    async.waterfall([
      function (waterfallCallback) {
        const url = `https://itunes.apple.com/search?media=music&entity=musicTrack&limit=500&term=${encodeURI(query)}`;
        curl(url, (result)=>{
          waterfallCallback(null, result);
        });
      },
      function (result, waterfallCallback) {
        let releasesFromSource = [];
        if (result.html) {
          releasesFromSource = $this.parseList(result.html);
        }
        if (releasesFromSource.length > 0) {
          async.each(releasesFromSource, (r, eachCallback) => {
            const release = diggingHelpers.prepareRelease($this, r);

            if (release) {
              releases.push(release);
            }
            eachCallback(null);
          }, () => {
            waterfallCallback(null);
          });
        } else {
          waterfallCallback(null);
        }
      }], () => {
      searchArtistCallback(releases);
    });
  }
  // query: string
  // searchArtistCallback: function receiving array of release objects
  all(query, searchArtistCallback) {
    const $this = this;
    const releases = [];
    async.waterfall([
      function (waterfallCallback) {
        const url = `https://itunes.apple.com/search?media=music&entity=musicTrack&limit=500&term=${encodeURI(query)}`;
        curl(url, (result)=>{
          waterfallCallback(null, result);
        });
      },
      function (result, waterfallCallback) {
        let releasesFromSource = [];
        if (result.html) {
          releasesFromSource = $this.parseList(result.html);
        }
        if (releasesFromSource.length > 0) {
          async.each(releasesFromSource, (r, eachCallback) => {
            const release = diggingHelpers.prepareRelease($this, r);

            if (release) {
              releases.push(release);
            }
            eachCallback(null);
          }, () => {
            waterfallCallback(null);
          });
        } else {
          waterfallCallback(null);
        }
      }], () => {
      searchArtistCallback(releases);
    });
  }
  // html : html source code
  parseList(html) {
    const data = JSON.parse(html);
    const releases = [];
    const releasesIndex = {};
    const allTracks = [];
    if (data.results && data.results.length > 0) {
      data.results.forEach((d) => {
        const releaseSourceId = d.collectionId;
        const trackId = d.trackId;
        const releaseDate = d.releaseDate;
        const exp = releaseDate.split('-');
        const exp1 = releaseDate.split('T');

        if (d.collectionViewUrl && (trackId !== releaseSourceId)) {
          allTracks.push({
            releaseTitle: d.collectionArtistName,
            trackTitle: d.trackName,
            releaseYear: exp[0],
            releaseDate: exp1[0],
            releaseCover: d.artworkUrl30,
            releaseLabel: null,
            releaseUrl: d.collectionViewUrl,
            releaseGenre: d.primaryGenreName,
            trackArtist: d.artistName,
            releaseSourceId,
            trackSourceId: d.trackId
          });
        }
      });
    }
    allTracks.forEach((t) => {
      const rid = t.releaseSourceId;

      if (!releasesIndex[rid]) {
        releasesIndex[rid] = {
          releaseArtist: t.trackArtist,
          releaseTitle: t.releaseTitle,
          releaseYear: t.releaseYear,
          releaseDate: t.releaseDate,
          releaseCover: t.releaseCover,
          releaseLabel: t.releaseLabel,
          releaseUrl: t.releaseUrl,
          releaseGenre: t.releaseGenre,
          releaseSourceId: t.releaseSourceId
        };
        releasesIndex[rid].tracks = [];
      }
      releasesIndex[rid].tracks.push({
        artist: t.trackArtist,
        title: t.trackTitle
      });
    });
    for (const rid in releasesIndex) {
      const r = releasesIndex[rid];
      releases.push({
        kind: 'release',
        artist: r.releaseArtist,
        tracks: r.tracks,
        label: r.releaseLabel,
        cover: r.releaseCover,
        source: 'itunes',
        url: r.releaseUrl,
        title: r.releaseTitle,
        playedBy: null,
        year: r.releaseYear,
        releaseDate: r.releaseDate,
        genre: r.releaseGenre
      });
    }
    return releases;
  }
  /* getNews: function(){

   }*/
};
