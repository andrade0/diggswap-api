const  Fuse = require('fuse.js');
const  _ = require('underscore');
const  async = require('async');
const  S = require('string');
const  cheerio = require('cheerio');
const  moment = require('moment');
const  curl = require('../../curl');
const  diggingHelpers = require('../diggingHelpers');


module.exports = class youtube {
  constructor() {
    this.name = 'Youtube';
    this.value = 'youtube';
    this.logo = 'public/img/sources/youtube.png';
    this.color = '#FF0000';
  }
  news(channelsURLs, getNewsCallback) {

    const channels = channelsURLs.map((channel) => {
      return channel.url.replace('https://www.youtube.com/channel/', '')
    });

    const $this = this;

    const all_items = [];
    async.each(channels, (channelId, cb) => {
      const url = 'https://www.googleapis.com/youtube/v3/search?order=date&part=snippet&channelId='+channelId+'&maxResults=50&key=AIzaSyDZNXZkEYA_VzTcKDk52F7WgCOcqHT1pdc';
      curl(url, function(result) {

        result.html.items.forEach(function(item){

          const release = {};
          release.id = md5(item.id+Math.random());
          const title = item.snippet.title;
          const expl = title.split(' - ');
          if(expl.length === 2)
          {
            release.title = expl[1];
            release.artist = expl[0];
          }
          else
          {
            release.title = title;
            release.artist = null;
          }
          release.label = item.snippet.channelTitle;
          release.source = 'youtube';
          release.genre = "house";
          release.playedBy = null;
          release.logo = $this.logo;
          release.cover = '/public/img/vinyl-digging-transparent.png';
          release.kind = 'release';
          release.year = moment(item.snippet.publishedAt).year();
          release.releaseDate = moment(item.snippet.publishedAt).unix();
          release.tracks = [
            {
              id: md5(item.id+Math.random()),
              artist: release.artist,
              title: release.title,
              logo: $this.logo,
              source : 'youtube',
              label: item.snippet.channelTitle,
              cover: '/public/img/vinyl-digging-transparent.png',
              youtube: item.id.videoId,
              item_type: 'track',
              source: 'Youtube',
              releaseTitle: release.title,
              genre: 'house',
              year: moment(item.snippet.publishedAt).year(),
              releaseDate: moment(item.snippet.publishedAt).unix()
            }
          ];

          const keywords_track = [release.title, release.artist, release.label, release.genre, release.playedBy, release.kind, release.source];
          release.query = keywords_track.join(' ');

          let alias = S(release.artist+'-'+release.title).latinise().s;
          alias = S(alias).strip("'", '"', '!','`','*','£','ù','%','$','¨','°','(',')','§','[',']','{','}',',').s;
          alias = S(alias).humanize().s;
          alias = alias.toLowerCase();
          alias = S(alias).replaceAll(' ', '-').s;
          alias = S(alias).replaceAll('http://www.mixesdb.com/w/', '').s;
          release.alias = alias;
          release.url = 'https://www.youtube.com/watch?v='+item.id.videoId;

          all_items.push(release);
        });
        cb(null);

      });
    }, (e, r) => {
      getNewsCallback(all_items);
    });
  }
};
