const  Fuse = require('fuse.js');
const  _ = require('underscore');
const  async = require('async');
const  S = require('string');
const  cheerio = require('cheerio');
const  moment = require('moment');
const  curl = require('../../curl');
const  diggingHelpers = require('../diggingHelpers');

module.exports = class ra {
  constructor() {
    this.name = 'Resident advisor';
    this.value = 'ra';
    this.logo = 'public/img/sources/ra.png';
    this.color = '#FCFB00';
    this.actions = ['playedBy'];
  }
  artist(query, searchArtistCallback) {
    const $this = this;
    const pages = [];

    let artistUrl = query;
    artistUrl = S(artistUrl).replaceAll('(', '').s;
    artistUrl = S(artistUrl).replaceAll(')', '').s;
    artistUrl = S(artistUrl).humanize().s;
    artistUrl = artistUrl.replace(/[^\w\s]/gi, '');
    artistUrl = S(artistUrl).replaceAll(' ', '').s;
    artistUrl = artistUrl.toLowerCase();
    const url = `https://www.residentadvisor.net/dj/${artistUrl}/top10`;

    async.waterfall([
      function (waterfallCallback) {
        curl(url, (result)=>{
          waterfallCallback(null, result);
        });
      },
      function (result, waterfallCallback) {
        if (result.html) {
          const $ = cheerio.load(result.html);
          $('li').each(function () {
            if ($(this) && $(this).attr('id') && $(this).attr('id').length > 0) {
              const expl = $(this).attr('id').split('-');
              if (expl.length === 2) {
                if (expl[0] === 'quantity') {
                  const playlistId = expl[1];
                  pages.push({url: `${url}?chart=${playlistId}`});
                }
              }
            }
          });
          waterfallCallback(null);
        }
      }
    ], (error, result) => {
      searchArtistCallback(pages);
    });
  }
  artist1(query, page, searchArtist1Callback) {
    const $this = this;
    async.waterfall([
      function (waterfallCallback) {
        curl(page.url, (result)=>{
          waterfallCallback(null, result)
        });
      },
      function (result, waterfallCallback) {
        if (result.release) {
          const release = diggingHelpers.prepareRelease($this, result.release);
          waterfallCallback(release);
        } else if (result.html) {
          const r = $this.parseDetail(result.html, page.url);

          if (r) {
            const release = diggingHelpers.prepareRelease($this, r);
            if (release) {
              waterfallCallback(release);
            } else {
              waterfallCallback(null);
            }
          } else {
            waterfallCallback(null);
          }
        } else {
          waterfallCallback(null);
        }
      }
    ], (err) => {
      searchArtist1Callback(err);
    });
  }
  parseDetail(html, url) {
    const $ = cheerio.load(html);
    const sample = null;
    let ep = '';
    const cover = null;
    const label = null;
    let releaseDate = null;
    const source = 'Resident advisor';
    const tracks = [];
    let year = null;
    let artist = null;

    artist = $('#sectionHead').find('h1').text().trim();

    if (!artist || artist && artist.length < 1) {
      artist = $('#featureHead').find('h1').text().trim();
    }

    $('ul#tracks li').each(function (k, v) {
      if (k === 1000) {

      } else {
        const track = {};
        $(this).find('div').each((k2, div) => {
          const classe = div.attribs.class;

          if (classe === 'artist') {
            if (div.children && div.children.length > 0 && div.children[0] && div.children[0].name && div.children[0].name === 'a') {
              track.artist = div.children[0].children[0].data;
            } else if (div.children && div.children[0] && div.children[0].data && div.children[0].data.length > 0) {
              track.artist = div.children[0].data;
            } else {
              track.artist = '';
            }
          } else if (classe === 'track') {
            if (div.children && div.children.length > 0 && div.children[0] && div.children[0].name && div.children[0].name === 'a') {
              track.title = div.children[0].children[0].data;
            } else if (div.children[0]) {
              track.title = div.children[0].data;
            } else {
              track.title = null;
            }
          } else if (classe === 'label') {
            if (div.children && div.children.length > 0 && div.children[0] && div.children[0].name && div.children[0].name === 'a') {
              track.label = div.children[0].children[0].data;
            } else if (div.children && div.children.length > 0) {
              track.label = div.children[0].data;
            }
          }
        });
        tracks.push(track);
      }
    });

    const months = [];
    months.push('January');
    months.push('February');
    months.push('March');
    months.push('April');
    months.push('May');
    months.push('June');
    months.push('July');
    months.push('August');
    months.push('September');
    months.push('October');
    months.push('November');
    months.push('December');
    const releaseDate_str = $('section.content div.plus8 div.clearfix.pb16 div.dropdown.but.arrow-down').find('span').text();
    if (releaseDate_str) {
      const sp = releaseDate_str.split(' ');
      if (sp && sp.length && sp.length === 2) {
        let m = _.indexOf(months, sp[0]);
        if (m > 0) {
          if (m < 10) {
            m = `0${m}`;
          }
          releaseDate = `${sp[1]}-${m}-` + '01';
        }
      }
    }

    $('main ul.content-list li section.content.clearfix div.plus8 div.clearfix.pb16 div.dropdown.but.arrow-down span').each((k, v) => {
      if (v.children && v.children[0] && v.children[0].data) {
        const split = v.children[0].data.split(' ');
        if (split && split[1]) {
          if (artist) {
            ep += `${artist} RA chart ${v.children[0].data}`;
          } else {
            ep += ` RA chart ${v.children[0].data}`;
          }

          year = split[1].trim();
        }
      }
    });

    if (tracks.length > 0) {
      return {
        kind: 'playlist',
        artist,
        tracks,
        label,
        cover,
        source,
        url,
        title: ep,
        playedBy: artist,
        year,
        genre: 'Electronic music',
        releaseDate
      };
    }

    return null;
  }
};
