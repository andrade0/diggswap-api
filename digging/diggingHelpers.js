const async = require('async');
const S = require('string');
const cheerio = require('cheerio');
const md5 = require('md5');
const moment = require('moment');
const Fuse = require('fuse.js');


class DiggingHelpers {
  prepareRelease(source, doc) {
    const $this = this;

    if (!doc.tms) {
      const tms = new Date().getTime();
      doc.tms = tms;
    }

    if (!doc.logo && source.logo) {
      doc.logo = source.logo;
    }

    if (!doc.item_type) {
      doc.item_type = 'release';
    }

    if (!doc.cover || (doc.cover && doc.cover.length <= 0)) {
      doc.cover = '/public/img/vinyl-digging-transparent.png';
    }

    if (doc.ep && !doc.title) {
      doc.title = doc.ep;
    }

    if (doc.artist) {
      doc.artist = doc.artist.replace(new RegExp('\[.*\]', 'i'), '');
      doc.artist = doc.artist.trim().toLowerCase();
      doc.artist = S(doc.artist).capitalize().s;
    }

    if (doc.playedBy) {
      doc.playedBy = doc.playedBy.trim().toLowerCase();
      doc.playedBy = S(doc.playedBy).capitalize().s;
    }

    if (doc.title) {
      doc.title = doc.title.trim().toLowerCase();
      doc.title = S(doc.title).capitalize().s;
    }

    if (doc.genre) {
      doc.genre = doc.genre.trim().toLowerCase();
      doc.genre = S(doc.genre).capitalize().s;
    }

    if (doc.label) {
      doc.label = doc.label.trim().toLowerCase();
      doc.label.replace('germany', '');
      doc.label.replace('france', '');
      doc.label.replace('japan', '');
      doc.label.replace('greece', '');
      doc.label.replace('spain', '');
      doc.label.replace('uk', '');
      doc.label.replace('italy', '');
      doc.label.replace('us', '');
      doc.label = S(doc.label).capitalize().s;
    }

    doc.source = source.value.trim().toLowerCase();

    const keywords = [doc.title, doc.artist, doc.label, doc.genre, doc.playedBy, doc.kind, doc.source];

    if (doc.url && doc.url.length && doc.url.length > 0 && doc.tracks && doc.tracks.length && doc.tracks.length > 0) {
      doc.tracks.forEach((v, k) => {
        if (v.title && v.artist && v.artist.trim().length > 0 && v.title.trim().length > 0) {
          v.title = v.title.replace(new RegExp('\[.*\]', 'i'), '');
          v.artist = v.artist.replace(new RegExp('\[.*\]', 'i'), '');

          v.artist = S(v.artist.trim()).capitalize().s;
          v.title = S(v.title.trim()).capitalize().s;

          if (!v.item_type) {
            v.item_type = 'track';
          }

          if (!v.logo && doc.logo) {
            v.logo = doc.logo;
          }

          v.releaseTitle = doc.title;

          v.tms = doc.tms;

          if (doc.label && !v.label) {
            v.label = doc.label;
          } else if (v.label) {
            v.label = S(v.label.trim()).capitalize().s;
          }

          if (v.label) {
            v.label = v.label.trim().toLowerCase();
            v.label.replace('germany', '');
            v.label.replace('france', '');
            v.label.replace('japan', '');
            v.label.replace('greece', '');
            v.label.replace('spain', '');
            v.label.replace('uk', '');
            v.label.replace('italy', '');
            v.label.replace('us', '');
            v.label = S(v.label).capitalize().s;
          }

          if (!v.duration) {
            v.duration = null;
          }

          if (doc.releaseDate) {
            v.releaseDate = moment(doc.releaseDate).unix();
          } else {
            v.releaseDate = moment().unix();
          }

          if (doc.genre && !v.genre) {
            v.genre = doc.genre;
          } else if (v.genre) {
            v.genre = S(v.genre.trim()).capitalize().s;
          }

          if (doc.playedBy && !v.playedBy) {
            v.playedBy = doc.playedBy;
          } else if (v.playedBy) {
            v.playedBy = S(v.playedBy.trim()).capitalize().s;
          }

          v.url = doc.url;

          v.releaseDate = doc.releaseDate;
          v.year = doc.year;
          v.kind = doc.kind;
          v.cover = doc.cover;
          v.source = doc.source;
          v.logo = source.logo;

          if (typeof v.year != "number") {
            v.year = null;
          }


          if (!v.youtube) {
            v.youtube = {};
          }

          keywords.push(v.title);

          if ((doc.artist && v.artist) && doc.artist.toLowerCase().trim() !== v.artist.toLowerCase().trim()) {
            keywords.push(v.artist);
          }

          const keywords_track = [doc.title, doc.artist, doc.label, doc.genre, doc.playedBy, doc.kind, doc.source, v.title, v.artist];

          if (!v.query) {
            v.query = keywords_track.join(' ');
          }

          let alias = S(`${v.artist}-${doc.title}-${v.title}`).latinise().s;
          alias = S(alias).strip("'", '"', '!', '`', '*', '£', 'ù', '%', '$', '¨', '°', '(', ')', '§', '[', ']', '{', '}', ',').s;
          alias = S(alias).humanize().s;
          alias = alias.toLowerCase();
          alias = S(alias).replaceAll(' ', '-').s;
          alias = S(alias).replaceAll('http://www.mixesdb.com/w/', '').s;

          if (!v.alias) {
            v.alias = alias;
          }

          if (!v.id) {
            v.id = md5(v.alias + k);
          }
        } else {
          delete doc.tracks[k];
        }
      });

      if (!doc.query) {
        doc.query = keywords.join(' ');
      }

      let alias = null;

      if (doc.artist) {
        alias = S(`${doc.artist}-${doc.title}`).latinise().s;
      } else {
        alias = S(doc.title).latinise().s;
      }

      alias = S(alias).strip("'", '"', '!', '`', '*', '£', 'ù', '%', '$', '¨', '°', '(', ')', '§', '[', ']', '{', '}', ',').s;
      alias = S(alias).humanize().s;
      alias = alias.toLowerCase();
      alias = S(alias).replaceAll(' ', '-').s;
      alias = S(alias).replaceAll('http://www.mixesdb.com/w/', '').s;

      if (!doc.alias) {
        doc.alias = alias;
      }

      doc.id = doc.alias;

      // $this.insert_release_in_parse([doc]);

      doc.youtube = {};

      return doc;
    }
    return null;
  }
}
const diggingHelpers = new DiggingHelpers();
module.exports = diggingHelpers;
