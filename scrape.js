const _ = require('underscore');
const async = require('async');
const sourcesObjects = require('./digging/sources');

const scrapeArtists = (artist, cb)=>{
    sourcesObjects.juno.artist(artist, (releases)=>{
        if(releases && releases.length && releases.length > 0){
            releases = releases.reduce((c, r)=>{
                if(r && r.artist && r.artist.match(new RegExp(artist, 'i'))){
                    if(r.tracks && r.tracks.length && r.tracks.length > 0){
                        r.tracks = r.tracks.reduce((cc, t)=>{
                            if(t && t.artist && t.artist.match(new RegExp(artist, 'i'))){
                                cc.push(t);
                            }
                            return cc;
                        }, []);
                        c.push(r);
                    }
                }
                return c;
            }, []);
            cb(releases);
        } else {
            cb([]);
        }
    });
};

const scrapeLabels = (label, cb)=>{
    sourcesObjects.juno.label(label, (releases)=>{
        if(releases && releases.length && releases.length > 0){
            releases = releases.reduce((c, r)=>{
                if(r && r.label && r.label.match(new RegExp(label, 'i'))){
                    c.push(r);
                }
                return c;
            }, []);
            cb(releases);
        } else {
            cb([]);
        }
    });
};

const scrapeClubs = (club, cb)=>{
    let releases = [];
    sourcesObjects.mixesdb.all(club, (pages)=>{
        if(pages && pages.length && pages.length > 0){
            async.eachSeries(pages, (page, cbEach)=>{
                sourcesObjects.mixesdb.all1(club, page, (release)=>{
                    if(release && release.title && release.title.match(new RegExp(club, 'i'))){
                        releases.push(release);
                    }
                    cbEach();
                });
            }, ()=>{
                cb(releases)
            });
        } else {
            cb(releases);
        }
    });
};

const scrapePlaylists = (artist, mainCB)=>{
    async.reduce(['ra', 'mixesdb'], [], (state, sourceName, reduceCb)=>{
        sourcesObjects[sourceName].artist(artist, (pages)=>{

            if(pages && pages.length && pages.length > 0){
                async.reduce(pages, state, (state2, page, cbEach)=>{
                    sourcesObjects[sourceName].artist1(artist, page, (release)=>{
                        if(release && ((release.playedBy && release.playedBy.match(new RegExp(artist, 'i'))) || (release.title && release.title.match(new RegExp(artist, 'i'))))){
                            cbEach(null, [...state2, release]);
                        } else {
                            cbEach(null, state2);
                        }
                    });
                }, (error, _releases)=>{
                    reduceCb(null, [...state, ..._releases]);
                });
            } else {
                reduceCb(null, state)
            }
        });
    }, (error, releases)=>{
        mainCB(releases);
    });
};

const scrapeNews = (sources, mainCb)=>{
    async.reduce(sources, [], (state, source, cbEachSources)=>{
        if(sourcesObjects[source.name] && sourcesObjects[source.name].news){
            sourcesObjects[source.name].news((releases)=>{
                if(source.genres && source.genres.length > 0 && sourcesObjects[source.name].newsHaveGenre){
                    releases = releases.reduce((_state, r)=>{
                        const insert = source.genres.reduce((s, g)=>{
                            if(s === false && r && r.genre && r.genre.match(new RegExp(g, 'i'))){
                                s = true;
                            }
                            return s;
                        }, false);
                        if(insert){
                            _state.push(r);
                        }
                        return _state;
                    }, []);
                    cbEachSources(null, [...state, ...releases]);
                } else {
                    cbEachSources(null, [...state, ...releases]);
                }
            });
        } else {
            cbEachSources();
        }
    }, (error, releases)=>{
        mainCb(releases);
    });
};

const scrape = (scraper, cb)=>{

    const source = scraper.source;
    let type = scraper.type;

    if(type === 'club'){
        type = 'all';
    }

    if(type === 'playedBy'){
        type = 'artist';
    }

    const query = scraper.query?scraper.query:null;
    const genre = scraper.genre?scraper.genre:null;
    const action = type;

    const releases = [];

    switch (source) {
        case 'all':
            async.each(sourcesObjects, (_source, callabck_each) => {
                let searchFuncName1 = action;
                let searchFuncName2 = `${action}_1`;
                const functions = [];
                if (_source[searchFuncName1]) {
                    functions.push(searchFuncName1);
                }
                if (_source[searchFuncName2]) {
                    functions.push(searchFuncName2);
                }

                if (functions.length === 1) {
                    _source[action](query, (_releases) => {
                        _releases.forEach((r) => {
                            releases.push(r);
                        });
                        callabck_each(null);
                    });
                } else if (functions.length === 2) {
                    let searchFuncName1 = functions[0];
                    let searchFuncName2 = functions[1];

                    _source[searchFuncName1](query, null, (pages) => {
                        if (pages.length > 0) {
                            async.each(pages, (page, eachPagesCallback) => {
                                _source[searchFuncName2](query, page, (release) => {
                                    if (release) {
                                        releases.push(release);
                                    }
                                    eachPagesCallback(null);
                                });
                            }, () => {
                                callabck_each(null);
                            });
                        } else {
                            callabck_each(null);
                        }
                    });
                } else {
                    callabck_each(null);
                }
            }, () => {
                cb(releases);
            });
            break;
        default:

            const sourceObject = _.findWhere(sourcesObjects, { value: source });

            if (sourceObject) {
                let searchFuncName1 = action;
                let searchFuncName2 = `${action}1`;
                const functions = [];
                if (sourceObject[searchFuncName1]) {
                    functions.push(searchFuncName1);
                }
                if (sourceObject[searchFuncName2]) {
                    functions.push(searchFuncName2);
                }

                if (functions.length === 1) {
                    sourceObject[action](query, (_releases) => {
                        _releases.forEach((r) => {
                            if(genre){
                                if(r.genre.match(new RegExp(genre, 'i'))){
                                    releases.push(r);
                                }
                            } else if(query && type === 'artist' && r.tracks && r.tracks.length > 0){
                                r.tracks = r.tracks.reduce((state, t)=>{
                                    if(t.artist && t.artist.match(new RegExp(query, 'i'))){
                                        state.push(t);
                                    }
                                    return state;
                                }, []);
                                if(r.tracks.length > 0){
                                    releases.push(r);
                                }
                            } else if(query && type === 'label'){
                                r.tracks = r.tracks.reduce((state, t)=>{
                                    if(t.label && t.label.match(new RegExp(query, 'i'))){
                                        state.push(t);
                                    }
                                    return state;
                                }, []);
                                if(r && r.tracks && r.tracks.length > 0){
                                    releases.push(r);
                                }
                            } else if (r && r.tracks && r.tracks.length > 0) {
                                releases.push(r);
                            }
                        });
                        cb(releases);
                    });
                } else if (functions.length === 2) {

                    let searchFuncName1 = functions[0];
                    let searchFuncName2 = functions[1];
                    sourceObject[searchFuncName1](query, null, (pages) => {
                        if (pages.length > 0) {
                            async.eachSeries(pages, (page, eachPagesCallback) => {
                                sourceObject[searchFuncName2](query, page, (r) => {
                                    if (r) {
                                        if(genre){
                                            if(r.genre.match(new RegExp(genre, 'i'))){
                                                releases.push(r);
                                            }
                                        } else if(query && type === 'artist' && r.tracks && r.tracks.length > 0){
                                            r.tracks = r.tracks.reduce((state, t)=>{
                                                if(t.artist && t.artist.match(new RegExp(query, 'i'))){
                                                    state.push(t);
                                                }
                                                return state;
                                            }, []);
                                            if(t.tracks.length > 0){
                                                releases.push(r);
                                            }
                                        } else if(query && type === 'label'){
                                            r.tracks = r.tracks.reduce((state, t)=>{
                                                if(t.label && t.label.match(new RegExp(query, 'i'))){
                                                    state.push(t);
                                                }
                                                return state;
                                            }, []);
                                            if(r && r.tracks && r.tracks.length > 0){
                                                releases.push(r);
                                            }
                                        } else if (r && r.tracks && r.tracks.length > 0) {
                                            releases.push(r);
                                        }
                                    }
                                    eachPagesCallback();
                                });
                            }, () => {
                                cb(releases);
                            });
                        } else {
                            cb(releases);
                        }
                    });
                } else {
                    cb(`Error: action "${action}" not set in Source "${source}" not found.`);
                }
            } else {
                cb(`Error: Source "${source}" not found.`);
            }
            break;
    }

};

module.exports = {
    scapre: scrape,
    artist: scrapeArtists,
    label: scrapeLabels,
    club: scrapeClubs,
    news: scrapeNews,
    playlist: scrapePlaylists
};
