import React, { Component, PropTypes } from "react";
import { observer } from 'mobx-react';
import RouterModel from './models/RouterModel';
import User from './models/User';
import Login from './Login';

@observer
class Router extends Component {
    constructor() {
        super();
    }

    componentDidMount(){
        let w = $( window ).width();
            let h = $( window ).height();
            $( "#wrapper" ).css( "width", w );
            $( "#wrapper" ).css( "height", h );
        $( window ).resize(function() {
            let w = $( window ).width();
            let h = $( window ).height();
            $( "#wrapper" ).css( "width", w );
            $( "#wrapper" ).css( "height", h );
        });
    }

    componentWillMount(){

    }

    componentWillUnmount() {

    }

    render() {
        if(User.token){
            if(RouterModel.currentScreen){
                const args = {title: RouterModel.currentScreen.title};
                if(RouterModel.currentScreen.arguments){
                    for(let index in RouterModel.currentScreen.arguments){
                        args[index] = RouterModel.currentScreen.arguments[index];
                    }
                }
                return (<div id="wrapper" style={{width: "100%", height: "100%", margin: 'auto', backgroundColor: '#222', border: 'solid 1px #423636', paddingBottom: 20}}>
                    <RouterModel.currentScreen.component {...args} />
                </div>)
            } else {
                return <div>Error no screen defenied in router</div>
            }
        } else {
            return <Login/>
        }


    }
}

Router.propTypes = {

};

export default Router;


