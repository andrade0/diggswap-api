import React, { Component, PropTypes } from "react";
import { observer } from 'mobx-react';
import {Password} from 'primereact/password';
import {InputText} from 'primereact/inputtext';
import {Button} from 'primereact/button';
import User from './models/User';

@observer
class Login extends Component {
    constructor() {
        super();
        this.state = {
            email: localStorage.getItem('email'),
            password: localStorage.getItem('password')
        }
    }
    componentDidMount(){
        let w = $( window ).width();
            let h = $( window ).height();
            $( "#wrapper" ).css( "width", w );
            $( "#wrapper" ).css( "height", h );
        $( window ).resize(function() {
            let w = $( window ).width();
            let h = $( window ).height();
            $( "#wrapper" ).css( "width", w );
            $( "#wrapper" ).css( "height", h );
        });
    }
    render() {
        return <div id="wrapper" style={{width: '100%', height: '100%', margin: 'auto', backgroundColor: '#222', marginBottom: 20}}>
            <div style={{paddingTop: 80, display: 'flex', flexDirection: 'column', width: '100%', position: 'absolute', justifyContent: 'space-evenly', alignItems: 'center', height: '100%'}}>
                <div className="p-inputgroup">
                   <span className="p-inputgroup-addon"><i className="pi pi-user"></i></span>
                    <InputText style={{width: 250}} value={this.state.email?this.state.email:''} placeholder="Email" onChange={(e)=>{
                        this.setState({email: e.target.value});
                        localStorage.setItem('email', e.target.value);
                    }} />
                </div>
                <div className="p-inputgroup">
                   <span className="p-inputgroup-addon"><i className="pi pi-key"></i></span>
                    <Password style={{width: 250}} value={this.state.password?this.state.password:''} placeholder="Password" onChange={(e)=>{
                        this.setState({password: e.target.value});
                        localStorage.setItem('password', e.target.value);
                    }} />
                </div>
                <Button style={{width: 250}} label="Login" className="p-button-rounded p-button-secondary" onClick={(event)=>{
                    User.login(this.state.email, this.state.password);
                    event.preventDefault();
                }} />
            </div></div>
    }
}

Login.propTypes = {

};

export default Login;


