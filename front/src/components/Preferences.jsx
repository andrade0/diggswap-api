import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { LinearProgress } from 'material-ui/Progress';
import ReactPlayer from 'react-player';
import jQuery from 'jquery';
import { withStyles } from 'material-ui/styles';

import { css } from 'react-emotion';
import { ClipLoader } from 'react-spinners';

const styles = theme => ({

});

@observer
class Preferences extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    render(){
        if(this.state.value){
            return (<select onChange={this.props.onChange}>{this.props.items.map((item)=>{
                if(this.state.value === item.value){
                    return <option key={item.value} selected value={item.value}>{item.name}</option>
                } else {
                    return <option key={item.value} value={item.value}>{item.name}</option>
                }
            })}</select>);
        } else {
            return (<select onChange={this.props.onChange}><option value={null}>none</option>{this.props.items.map((item)=>{
                return <option key={item.value} value={item.value}>{item.name}</option>
            })}</select>);
        }
    }
}

export default withStyles(styles)(Preferences)

