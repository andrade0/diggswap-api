import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { LinearProgress } from 'material-ui/Progress';
import ReactPlayer from 'react-player';
import jQuery from 'jquery';
import { withStyles } from 'material-ui/styles';
import {InputText} from 'primereact/inputtext';
import DiggswapSelect from '../components/DiggswapSelect';
import App from '../models/App';
import Listes from '../models/Listes';

import { css } from 'react-emotion';
import { ClipLoader } from 'react-spinners';
import User from "../models/User";

const styles = theme => ({

});

const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

@observer
class AddListe extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            newScraperFormVisible: false,
            newScraperSource: null,
            newScraperAction: null,
            newScraperQuery: null,
            newScraperGenre: null,
            scraperOptions: null,
            scapers: []
        };
    }
    componentWillMount(){
        fetch(App.server+'/getScraperOptions?token='+User.token, {
            method: 'get',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            }
        }).then(res=>res.json())
            .then(res => {
                this.setState({scraperOptions: res})
            });
    }
    render(){

        if(this.state.scraperOptions){

            const typeOptions = Object.keys(this.state.scraperOptions).reduce((state, type)=>{
                state.push({name: type, value: type});
                return state;
            }, []);

            let sourcesSelect = null;
            if(this.state.newScraperAction){
                let sourcesOptions = [];
                if(this.state.newScraperAction === 'news'){
                    sourcesOptions = this.state.scraperOptions[this.state.newScraperAction].reduce((state, item)=>{
                        state.push({name: item.source, value: item.source});
                        return state;
                    }, []);
                } else {
                    sourcesOptions = this.state.scraperOptions[this.state.newScraperAction].reduce((state, source)=>{
                        state.push({name: source, value: source});
                        return state;
                    }, []);
                }
                sourcesSelect = <DiggswapSelect onChange={(e)=>{
                    this.setState({newScraperSource: e.target.value, newScraperGenre: null, newScraperQuery: null});
                }} value={this.state.newScraperSource} items={sourcesOptions} placeholder="Select an action"/>
            }

            let thirdActionSelect = null;
            if(this.state.newScraperSource && this.state.newScraperAction){
                if(this.state.newScraperAction === 'news'){

                    const newsHaveGenre = this.state.scraperOptions[this.state.newScraperAction].reduce((state, s)=>{
                        if(s.source === this.state.newScraperSource && s.newsHaveGenre){
                            state = true;
                        }
                        return state;
                    }, false);

                    if(newsHaveGenre){
                        thirdActionSelect = <DiggswapSelect onChange={(e)=>{
                            this.setState({newScraperGenre: e.target.value});
                        }} value={this.state.newScraperGenre} items={[
                            {name: 'All', value: 'all'},
                            {name: 'House', value: 'house'},
                            {name: 'Minimal', value: 'minimal'},
                            {name: 'Techouse', value: 'techouse'},
                            {name: 'Techno', value: 'techno'},
                            {name: 'Hiphop', value: 'hiphop'},
                            {name: 'Disco', value: 'disco'}
                        ]} placeholder="Select a genre"/>
                    }
                } else {
                    thirdActionSelect = <InputText value={this.state.newScraperQuery?this.state.newScraperQuery:''} onChange={(e) => this.setState({newScraperQuery: e.target.value})} />
                }
            }

            const scraperForm = <div>
                <DiggswapSelect onChange={(e)=>{
                    this.setState({newScraperAction: e.target.value, newScraperSource: null, newScraperGenre: null, newScraperQuery: null});
                }} value={this.state.newScraperSource} items={typeOptions} placeholder="Select a action"/>
                {sourcesSelect}
                {thirdActionSelect}
                <button onClick={()=>{
                    let valid = false;
                    if(this.state.newScraperSource && this.state.newScraperAction){
                        if(this.state.newScraperAction === 'news'){

                            const newsHaveGenre = this.state.scraperOptions[this.state.newScraperAction].reduce((state, s)=>{
                                if(s.source === this.state.newScraperSource && s.newsHaveGenre){
                                    state = true;
                                }
                                return state;
                            }, false);

                            if(newsHaveGenre){
                                if(this.state.newScraperGenre){
                                    valid = true;
                                }
                            } else {
                                valid = true;
                            }
                        } else {
                            if(this.state.newScraperQuery && this.state.newScraperQuery.length > 2){
                                valid = true;
                            }
                        }
                    }
                    if(valid){
                        let scapers = this.state.scapers.map((s)=>{
                            return s;
                        });
                        let newScrape = {
                            source: this.state.newScraperSource,
                            type: this.state.newScraperAction
                        };

                        if(this.state.newScraperAction === 'news'){
                            newScrape.genre = this.state.newScraperGenre;
                        } else {
                            newScrape.query = this.state.newScraperQuery;
                        }

                        scapers.push(newScrape);

                        this.setState({scapers, newScraperSource: null, newScraperAction: null, newScraperQuery: null, newScraperGenre: null});
                    }
                }}>
                    Add
                </button>
                <ul>{this.state.scapers.map((s)=>{
                    return <li key={`${s.source}${s.type}${s.genre}${s.query}`}>{s.source} {s.type} {s.genre} {s.query}</li>;
                })}</ul>
            </div>;

            return (
                <div>
                    <InputText value={this.state.name} onChange={(e) => this.setState({name: e.target.value})} />
                    <img onClick={()=>{this.setState({newScraperFormVisible: !this.state.newScraperFormVisible})}} style={{display: 'block', marginBottom: 20}} width={20} width={20} src={`http://diggswap.eu:3000/public/img/plus.png`} />
                    {this.state.newScraperFormVisible?scraperForm:null}
                    <button onClick={()=>{
                        if(this.state.name && this.state.name.length > 4){

                            fetch(App.server+'/liste?token='+User.token, {
                                method: 'post',
                                headers: {
                                    'Accept': 'application/json, text/plain, */*',
                                    'Content-Type': 'application/json'
                                },
                                body: JSON.stringify({title: this.state.name, scrapers: this.state.scapers})
                            }).then(res=>res.json())
                                .then(newListe => {
                                    let newListes = Listes.listes.map((l)=>{
                                        return l;
                                    });
                                    newListes.push(newListe);
                                    Listes.listes = newListes;
                                });
                        }
                    }}>Save</button>
                </div>

            );
        } else {
            return (<div>Loading</div>);
        }

    }
}

export default withStyles(styles)(AddListe)

