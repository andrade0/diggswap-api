import React, {Component} from 'react';
import {observer} from 'mobx-react';
import ReactPlayer from 'react-player';
import jQuery from 'jquery';
import {withStyles} from 'material-ui/styles';
import {Menu} from 'primereact/menu';
import {css} from 'react-emotion';
import {ClipLoader} from 'react-spinners';
import User from "../models/User";
import RouterModel from "../models/RouterModel";
import AppModel from "../models/App";
import {ScrollPanel} from 'primereact/scrollpanel';

const styles = theme => ({});

const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

@observer
class Liste extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tracks: null,
            count: 0,
            progress: 0,
            buffer: 0,
            playing: false,
            loading: true
        };
    }

    componentWillMount() {
        AppModel.request('tracks', 'get', {listeType: this.props.listeType}, (res)=>{
            this.setState({tracks: res, loading: false, count: res.length});
        });
    }

    render() {


        const {classes} = this.props;
        if (this.state.tracks && this.state.tracks.length > 0 && this.state.loading === false) {

            return (
                <div>
                    <div style={{
                        display: "flex",
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        width: '100%',
                        marginBottom: 20
                    }}>
                        <i style={{color: '#fff', fontSize: 25, marginLeft: 15, marginTop: 10}}
                           className="pi pi-fw pi-arrow-left" onClick={() => {
                            RouterModel.goto('Home');
                        }}></i>
                        <div style={{
                            fontFamily: "'Thasadith', sans-serif",
                            borderBottomLeftRadius: '20px',
                            borderTopRightRadius: '28px',
                            padding: 10,
                            backgroundColor: '#fff',
                            color: '#222',
                            marginRight: 0,
                            marginTop: 0
                        }}>{this.state.count} tracks
                        </div>
                    </div>

                    <ScrollPanel style={{width: '100%', height: '440px', marginTop: 20, fontSize: 12, fontFamily: "'Thasadith', sans-serif"}}>
                        <Menu style={{width: 400}} model={this.state.tracks.map((track) => {
                            let str = track.artist + ' - ' + track.title;
                            if(track.label){
                                str += ' ('+track.label+')';
                            }
                            return {
                                label: str,
                                icon: 'pi pi-fw pi-chevron-right',
                                command: () => {
                                    RouterModel.goto('Play', {tid: track._id, listeType: this.props.listeType, listeTitle: this.props.title})
                                }
                            };
                        })} popup={false} ref={el => this.menu = el}/>
                    </ScrollPanel>
                </div>
            );
        } else {
            return (
                <div style={{
                    height: '100%',
                    padding: 20,
                    backgroundColor: '#222',
                    display: "flex",
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center'
                }} className='sweet-loading'>
                    <ClipLoader
                        className={override}
                        sizeUnit={"px"}
                        size={150}
                        color={'#123abc'}
                        loading={this.state.loading}
                    />
                </div>
            )
        }
    }
}

export default withStyles(styles)(Liste)

