import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { withStyles } from 'material-ui/styles';
import AppModel from '../models/App';
import RouterModel from '../models/RouterModel';
import User from '../models/User';
import {Menu} from 'primereact/menu';
import {Button} from 'primereact/button';

import { css } from 'react-emotion';
import { ClipLoader } from 'react-spinners';

const styles = theme => ({

});

const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

@observer
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listes: [
                {listeType: 'news', title: 'News from records shops'},
                {listeType: 'djs', title: 'Music played by Djs'},
                {listeType: 'producers', title: 'Music from artists'},
                {listeType: 'clubs', title: 'Music from clubs'},
                {listeType: 'labels', title: 'Music from labels'},
            ],
            loading: false
        };
    }
    componentDidMount(){

    }
    render(){
        const { classes } = this.props;
        if(this.state.listes.length > 0 && this.state.loading === false){
            return <div style={{display: 'flex', flexDirection: 'column', width: '100%', position: 'absolute', justifyContent: 'space-between', alignItems: 'center', height: '100%'}}>
                <Menu style={{width: '100%', marginTop: 80}} model={this.state.listes.map((liste)=>{
                        return {label: liste.title, icon: 'pi pi-fw pi-arrow-right',command:()=>{ RouterModel.goto('Swap', {listeType: liste.listeType, listeTitle: liste.title}) }};
                })} popup={false} ref={el => this.menu=el} />
                <Button style={{marginBottom: 30}} onClick={()=>{
                    User.logout();
                }} label="Logout" className="p-button-rounded p-button-secondary" />
            </div>;
        } else {
            return (
                <div style={{height: '100%', padding: 20, backgroundColor: '#222', display: "flex", flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}} className='sweet-loading'>
                    <ClipLoader
                        className={override}
                        sizeUnit={"px"}
                        size={150}
                        color={'#123abc'}
                        loading={this.state.loading}
                    />
                </div>
            )
        }
    }
}

export default withStyles(styles)(Home)

