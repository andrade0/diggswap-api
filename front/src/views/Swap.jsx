import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { LinearProgress } from 'material-ui/Progress';
import ReactPlayer from 'react-player';
import jQuery from 'jquery';
import moment from 'moment';
import { withStyles } from 'material-ui/styles';
import AppModel from '../models/App';
import RouterModel from '../models/RouterModel';
import User from '../models/User';

import { css } from 'react-emotion';
import { ClipLoader } from 'react-spinners';
import {Button} from "primereact/button";

const styles = theme => ({

});

const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

@observer
class Swap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            track: null,
            count: 0,
            liked: 0,
            duration: 0,
            progress: 0,
            buffer: 0,
            playing: false,
            loading: true,
            url: null
        };
        this.sendAnswer = this.sendAnswer.bind(this);
    }
    sendAnswer(status){
        this.setState({loading: true, playing: false, progress: 0});
        AppModel.request('swap', 'post', {trackId: this.state.track._id, swapStatus: status}, (res)=>{
            if(res && res.status && res.status === 'error'){
                RouterModel.goto('Liste', {listeType: this.props.listeType});
            } else {
                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    res.playing = false;
                } else {
                    res.playing = true;
                }
                res.loading = false;
                this.setState(res);
            }
        });
    }
    componentWillMount(){
        AppModel.request('get', 'post', {listeType: this.props.listeType}, (res)=>{
            if(res && res.status && res.status === 'error'){
                RouterModel.goto('Liste', {listeType: this.props.listeType});
            } else {
                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    res.playing = false;
                } else {
                    res.playing = true;
                }
                res.loading = false;
                this.setState(res);
            }
        });
    }
    render(){
        const { classes } = this.props;
        if(this.state.track && this.state.loading === false){
            let url = null;
            let youtube = false;
            if(this.state.track.sample){
                url = this.state.track.sample;
            } else if(this.state.track.youtube && this.state.track.youtube.id) {
                youtube = true;
                //url = 'https://www.youtube.com/embed/'+this.state.track.youtube.id;
                url = 'https://www.youtube.com/embed/'+this.state.track.youtube.id+'?rel=0&modestbranding=1&mute=0&showinfo=0&controls=1&autoplay=1';
            }
            if(url){
                if(youtube){
                    return (
                        <div style={{ display: "flex", flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
                            <div style={{ display: "flex", flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', marginBottom: 20}}>
                                <i style={{color: '#fff', fontSize: 25, marginLeft: 15, marginTop: 10}} className="pi pi-fw pi-arrow-left" onClick={()=>{
                                    RouterModel.goto('Home');
                                }}></i>
                                <div style={{fontFamily: "'Thasadith', sans-serif", borderBottomLeftRadius: '20px', padding: 10, backgroundColor: '#fff', color: '#222', marginRight: 0, marginTop: 0}}>{this.state.count} tracks remaining</div>
                            </div>
                            <h3 style={{color: '#fff', marginTop: 0, fontFamily: "'Thasadith', sans-serif", fontWeight: 'normal'}}>{this.props.listeTitle}</h3>
                            <img style={{display: 'block', marginBottom: 0}} width={40} src={`http://diggswap.eu:3000/${this.state.track.logo}`} />
                            <iframe style={{marginTop: 20, marginBottom: 40}} width="260" height="160" src={url}
                                    frameBorder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen></iframe>
                            <div style={{ fontFamily: "'Thasadith', sans-serif", fontSize: 13, textTransform: 'uppercase', display: "flex", flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: 350, marginBottom: 5}}>
                                <div><span style={{color: "#fff"}}>Title: </span><span style={{color: "#727272"}}>{this.state.track.title}</span></div>
                                <div><span style={{color: "#fff"}}>Artist: </span><span style={{color: "#727272", marginTop: 10}}>{this.state.track.artist}</span></div>
                            </div>
                            <div style={{ fontFamily: "'Thasadith', sans-serif", fontSize: 13, textTransform: 'uppercase', display: "flex", flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: 350, marginBottom: 5}}>
                                <div><span style={{color: "#fff"}}>Genre: </span><span style={{color: "#727272"}}>{this.state.track.genre}</span></div>
                                <div><span style={{color: "#fff"}}>Played by: </span><span style={{color: "#727272", marginTop: 10}}>{this.state.track.playedBy}</span></div>
                            </div>
                            <div style={{ fontFamily: "'Thasadith', sans-serif", fontSize: 13, textTransform: 'uppercase', display: "flex", flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: 350, marginBottom: 5}}>
                                <div><span style={{color: "#fff"}}>Year: </span><span style={{color: "#727272"}}>{this.state.track.year}</span></div>
                                <div><span style={{color: "#fff"}}>Release date: </span><span style={{color: "#727272", marginTop: 10}}>{moment(this.state.track.releaseDate).format('DD/MM/YY')}</span></div>
                            </div>
                            <div style={{ fontFamily: "'Thasadith', sans-serif", fontSize: 13, textTransform: 'uppercase', display: "flex", flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: 350, marginBottom: 30}}>
                                <div><span style={{color: "#fff"}}>Release title: </span><span style={{color: "#727272", marginTop: 10}}>{this.state.track.releaseTitle}</span></div>
                                <div><span style={{color: "#fff"}}>Label: </span><span style={{color: "#727272", marginTop: 10}}>{this.state.track.label}</span></div>
                            </div>

                            <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center', padding: 10}}>
                                <img style={{display: 'block', margin: '10px', cursor: 'pointer'}} onClick={()=>{this.sendAnswer(1);}} width={30} height={30} src={`http://diggswap.eu:3000/public/img/like.png`} />

                                <img style={{display: 'block', margin: '10px', cursor: 'pointer'}} onClick={()=>{this.sendAnswer(2);}} width={30} height={30} src={`http://diggswap.eu:3000/public/img/dislike.png`} />
                            </div>

                            <div style={{ display: "flex", flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', marginBottom: 20}}>
                                <i style={{cursor: 'pointer', fontSize: 25, marginLeft: 15, marginTop: 10, color: '#000', borderRadius: 90, backgroundColor: '#ccccccf7'}} className="pi pi-fw pi-pencil" onClick={()=>{
                                    RouterModel.goto('Liste', {lid: this.props.lid, listeTitle: this.props.listeTitle});
                                }}></i>
                                <Button style={{width: 100, fontFamily: "'Thasadith', sans-serif", marginRight: 10, marginBottom: -5}} label={`${this.state.liked} Matches`} className="p-button-rounded p-button-secondary" onClick={(event)=>{
                                    RouterModel.goto('Liste', {lid: this.props.lid});
                                }} />
                            </div>
                        </div>
                    );
                } else {
                    return (
                        <div style={{ display: "flex", flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}}>
                            <div style={{ display: "flex", flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', marginBottom: 20}}>
                                <i style={{color: '#fff', fontSize: 25, marginLeft: 15, marginTop: 10}} className="pi pi-fw pi-arrow-left" onClick={()=>{
                                    RouterModel.goto('Home');
                                }}></i>
                                <div style={{fontFamily: "'Thasadith', sans-serif", borderBottomLeftRadius: '20px', padding: 10, backgroundColor: '#fff', color: '#222', marginRight: 0, marginTop: 0}}>{this.state.count} tracks remaining</div>
                            </div>
                            <h3 style={{color: '#fff', marginTop: 0, fontFamily: "'Thasadith', sans-serif", fontWeight: 'normal'}}>{this.props.listeTitle}</h3>
                            <img style={{display: 'block', marginBottom: 0}} width={40} src={`http://diggswap.eu:3000/${this.state.track.logo}`} />
                            <img style={{display: 'block', marginBottom: 20, marginTop: 20}} width={160} src={this.state.track.cover === '/public/img/vinyl-digging-transparent.png'?`http://diggswap.eu:3000/public/img/vinyl-digging-transparent.png`:this.state.track.cover} />

                            <div style={{ fontFamily: "'Thasadith', sans-serif", fontSize: 13, textTransform: 'uppercase', display: "flex", flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: 350, marginBottom: 5}}>
                                <div><span style={{color: "#fff"}}>Title: </span><span style={{color: "#727272"}}>{this.state.track.title}</span></div>
                                <div><span style={{color: "#fff"}}>Artist: </span><span style={{color: "#727272", marginTop: 10}}>{this.state.track.artist}</span></div>
                            </div>
                            <div style={{ fontFamily: "'Thasadith', sans-serif", fontSize: 13, textTransform: 'uppercase', display: "flex", flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: 350, marginBottom: 5}}>
                                <div><span style={{color: "#fff"}}>Genre: </span><span style={{color: "#727272"}}>{this.state.track.genre}</span></div>
                                <div><span style={{color: "#fff"}}>Played by: </span><span style={{color: "#727272", marginTop: 10}}>{this.state.track.playedBy}</span></div>
                            </div>
                            <div style={{ fontFamily: "'Thasadith', sans-serif", fontSize: 13, textTransform: 'uppercase', display: "flex", flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: 350, marginBottom: 5}}>
                                <div><span style={{color: "#fff"}}>Year: </span><span style={{color: "#727272"}}>{this.state.track.year}</span></div>
                                <div><span style={{color: "#fff"}}>Release date: </span><span style={{color: "#727272", marginTop: 10}}>{moment(this.state.track.releaseDate).format('DD/MM/YY')}</span></div>
                            </div>
                            <div style={{ fontFamily: "'Thasadith', sans-serif", fontSize: 13, textTransform: 'uppercase', display: "flex", flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: 350, marginBottom: 5}}>
                                <div><span style={{color: "#fff"}}>Release title: </span><span style={{color: "#727272", marginTop: 10}}>{this.state.track.releaseTitle}</span></div>
                                <div><span style={{color: "#fff"}}>Label: </span><span style={{color: "#727272", marginTop: 10}}>{this.state.track.label}</span></div>
                            </div>
                            <div style={{ fontFamily: "'Thasadith', sans-serif", fontSize: 13, textTransform: 'uppercase', display: "flex", flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: 350, marginBottom: 5}}>
                                <div><span style={{color: "#fff"}}>Sample: </span><span style={{color: "#727272", marginTop: 10}}>{this.state.track.sample}</span></div>
                            </div>

                            <ReactPlayer
                                ref={(player) => {
                                    return this.player = player;
                                }}
                                className='react-player'
                                style={{
                                    width: 241,
                                    height: 43,
                                    position: 'fixed',
                                    zIndex: 9999999999999999999999999,
                                    left: 0,
                                    bottom: 0,
                                    visibility: 'hidden'
                                }}
                                width={241}
                                height={43}
                                url={url}
                                playing={this.state.playing}
                                playbackRate={1}
                                progressInterval={500}
                                volume={1}
                                config={{
                                    youtube: {
                                        playerVars: { showinfo: 1, origin: 'http://localhost:8080/', enablejsapi: 1 }
                                    }
                                }}
                                onReady={function () {

                                }}
                                onStart={function () {

                                }}
                                onPlay={function () {

                                }}
                                onPause={function () {

                                }}
                                onBuffer={function () {

                                }}
                                onEnded={() => {
                                    this.sendAnswer(2);
                                }}
                                onError={() => {

                                }}
                                onProgress={(arg)=>{
                                    const width = jQuery('#progressBarDigswap').width(); //largeur de la progress bar en pixels
                                    const duration = this.state.duration; // durée du tracken secondes
                                    const played = arg.playedSeconds; //durée played en secondes
                                    let progress_in_pixels = (width*played)/duration;
                                    let progress = (progress_in_pixels*100)/width;
                                    this.setState({
                                        progress,
                                        buffer: arg.loaded*100,
                                        playing: true
                                    });
                                }}
                                onDuration={(duration) => {
                                    this.setState({duration: duration})
                                }}
                            />

                            <div id="progressBarDigswap" onClick={(e)=>{
                                const duration = this.state.duration;
                                const parentOffset = jQuery("#progressBarDigswap").offset();
                                const relX = e.pageX - parentOffset.left;
                                const width = jQuery('#progressBarDigswap').width();
                                const progress = (relX*duration)/width;
                                this.player.seekTo(progress);
                            }} style={{zIndex: 1, cursor: 'pointer', marginTop: 10, height: '10px', width: '90%', backgroundColor: '#CCC', maginLeft: 10, maginRight: 10}}>
                                <div style={{zIndex: 0, height: '10px', backgroundColor: '#000', width: this.state.progress+'%'}}>
                                    <div style={{zIndex: -1, height: '10px', backgroundColor: 'transparent', width: this.state.buffer+'%'}}>
                                    </div>
                                </div>
                            </div>

                            <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center', padding: 10}}>
                                <img style={{display: 'block', margin: '10px', cursor: 'pointer'}} onClick={()=>{this.sendAnswer(1);}} width={30} height={30} src={`http://diggswap.eu:3000/public/img/like.png`} />
                                <img style={{display: 'block', margin: '10px', cursor: 'pointer'}} onClick={()=>{
                                    this.setState({playing: !this.state.playing});
                                }} width={50} height={50} src={this.state.playing?`http://diggswap.eu:3000/public/img/pause.png`:`http://diggswap.eu:3000/public/img/play.png`} />
                                <img style={{display: 'block', margin: '10px', cursor: 'pointer'}} onClick={()=>{this.sendAnswer(2);}} width={30} height={30} src={`http://diggswap.eu:3000/public/img/dislike.png`} />
                            </div>

                            <div style={{ position: 'fixed', bottom: 0, display: "flex", flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', marginBottom: 20}}>
                                <i style={{cursor: 'pointer', fontSize: 25, marginLeft: 15, marginTop: 10, color: '#000', borderRadius: 90, backgroundColor: '#ccccccf7'}} className="pi pi-fw pi-pencil" onClick={()=>{
                                    RouterModel.goto('Liste', {lid: this.props.lid, listeTitle: this.props.listeTitle});
                                }}></i>
                                <Button style={{width: 100, fontFamily: "'Thasadith', sans-serif", marginRight: 10, marginBottom: -5}} label={`${this.state.liked} Matches`} className="p-button-rounded p-button-secondary" onClick={(event)=>{
                                    RouterModel.goto('Liste', {listeType: this.props.listeType});
                                }} />
                            </div>

                        </div>
                    );
                }
            } else {
                this.sendAnswer(2);
                return (<div>No media found for this track</div>);
            }

        } else {
            return (
                <div style={{height: '100%', padding: 20, backgroundColor: '#222', display: "flex", flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}} className='sweet-loading'>
                    <ClipLoader
                        className={override}
                        sizeUnit={"px"}
                        size={150}
                        color={'#123abc'}
                        loading={this.state.loading}
                    />
                </div>
            )
        }
    }
}

export default withStyles(styles)(Swap)

