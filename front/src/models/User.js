import { observable } from "mobx";
import AppModel from './App';

class User {
    @observable token = null;
    server = "http://digging.pt:3000"; // http://french-connect-dev.eu:3000 //http://localhost:3000
    constructor(){
        this.token = localStorage.getItem('token');

        this.request('checkToken', 'GET', {}, (responseJson)=>{
            if(responseJson && responseJson.status && responseJson.status === 'error'){
                this.logout();
            }
        });

    }
    request(path, method, payload, cb){

        let options = {
            method: method,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            }
        };

        if(method.toLowerCase().trim() !== 'get'){
            options.body = JSON.stringify(payload);
        }

        fetch(this.server+'/'+path+'?token='+this.token, options).then(res=>res.json())
            .then(res => {
                if(res && res.status && res.status === 'error' && res.message && res.message === 'unauthorized'){
                    this.logout();
                }
                cb(res);
            });
    }
    login(email, password){
        const payload = {email, password};
        fetch(AppModel.server+'/login', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(payload)
        }).then((response) => {
            if(response.status === 200){
                return response.json();
            } else {
                return response.statusText;
            }
        }).then((responseJson) => {
            if(responseJson && responseJson.token){
                this.token = responseJson.token;
                localStorage.setItem('token', this.token);
            } else {
                console.log(responseJson);
            }
        });
    }
    logout(){
        this.token = null;
        localStorage.removeItem('token');
    }
}

const _User = new User();
export default _User;
