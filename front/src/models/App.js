import User from "./User";

class App {
    server = "http://digging.pt:3000"; // http://french-connect-dev.eu:3000 //http://localhost:3000
    request(path, method, payload, cb){

        let query = '';

        let options = {
            method: method,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            }
        };

        if(method.toLowerCase().trim() !== 'get'){
            options.body = JSON.stringify(payload);
        } else {
            for(let index in payload){
                query += '&'+index+'='+payload[index];
            }
        }

        fetch(this.server+'/'+path+'?token='+User.token+query, options).then(res=>res.json())
            .then(res => {
                if(res && res.status && res.status === 'error' && res.message && res.message === 'unauthorized'){
                    User.logout();
                }
                cb(res);
            });
    }
}

const _App = new App();

export default _App;
