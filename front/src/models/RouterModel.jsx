import { observable, action, intercept, computed, toJS } from 'mobx';

import Home from '../views/Home';
import Swap from '../views/Swap';
import Play from '../views/Play';
import Liste from '../views/Liste';
import AddListe from '../views/AddListe';
import Settings from '../views/Settings';


class RouterModel {

  screens = [
    {
      key: "Home",
      component: Home,
      arguments: null
    },
    {
      key: "Swap",
      component: Swap,
      arguments: {lid: null}
    },
    {
      key: "Play",
      component: Play,
      arguments: {tid: null}
    },
    {
      key: "Liste",
      component: Liste,
      arguments: {lid: null}
    },
    {
      key: "AddListe",
      component: AddListe,
      arguments: null
    },
    {
      key: "Settings",
      component: Settings,
      arguments: null
    }
  ];
  @observable currentScreen = null;
  @observable previewsScreen = null;

  constructor() {
    this.currentScreen = this.screens.reduce((c, v)=>{
      if(v.key === "Home"){
        c = v;
      }
      return c
    }, null);
  }

  goto(routeKey, args){

    this.previewsScreen = this.screens.reduce((c, v)=>{
      if(v.key === this.currentScreen.key){
        c = v;
      }
      return c
    }, null);

    this.currentScreen = this.screens.reduce((c, v)=>{
      if(v.key === routeKey){
        if(args){
          v.arguments = args;
        }
        c = v;
      }
      return c
    }, null);
  }

  back(){
    if(this.previewsScreen && this.previewsScreen.key){
      this.goto(this.previewsScreen.key, null);
    }
  }

}

const _RouterModel = new RouterModel();
export default _RouterModel;
