import { observable, computed, action } from "mobx";

class Listes {
  @observable listes = []
}

const _Listes = new Listes();
export default _Listes;
