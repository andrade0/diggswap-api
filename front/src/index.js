import React from "react";
import { render } from "react-dom";
import DevTools from "mobx-react-devtools";

import Router from "./Router";

$(function() {
    render(
        <div>
            <Router />
        </div>,
        document.getElementById("root")
    );

    setInterval(()=>{
        $( window ).resize(function() {
            let w = $( window ).width();
            let h = $( window ).height();
            $( "#wrapper" ).css( "width", w );
            $( "#wrapper" ).css( "height", h );
        });
    }, 500);
});






// playing around in the console

